+++
title ="Week 06: 3D Scanning and Printing"
+++
<img class="img-fluid pb-3" src="../present.jpg" alt="present">


## Assignment
- Group assignment:
	- test the design rules for your 3D printers
- Individual assignment:
	- design and 3D print an object that could not be made **subtractively**
    - 3D scan an object and optionally print it

## Group Assignment

- Group Members: Daniel Wilenius, Dann Mensah, 吳若玄 (Russian Wu)

We compared three 3D printers: **Lulzbot Mini, Ultimaker 2+ and Ultimaker S3**

  <div class="row pb-4">
    <div class="col">
      <img class="img-fluid py-1" src="../compare1.JPG" alt="compare1">
      <b>Lulzbot Mini</b>
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../compare2.JPG" alt="compare2">
      <b>Ultimaker 2+</b>
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../compare3.JPG" alt="compare3">
      <b>Ultimaker S3</b>
    </div>
  </div>
 

#### Lulzbot Mini
	
The Lulzbot Mini is the least precise of the three. It has a minimum layer thickness of **0.05mm and a 0.5mm** nozzle. It can programmatically adjust the baseplate to be flat using the four sensors in the corners of the plate. On the software side, Cura Lulzbot is used to prepare the prints and output the **.gcode files**.


  <div class="row pb-4">
    <div class="col">
      <img class="img-fluid py-1" src="../compare4.JPG" alt="compare4">
      ↑In the overhang test the Mini was able to print overhangs of up to <b>40 degrees</b> without any of the PLA running down. 
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../compare5.JPG" alt="compare5">
      ↑→The bridging test showed no problems on bridges of up to <b>20mm</b>. 
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../compare6.JPG" alt="compare6">
    </div>
  </div>


#### Ultimaker 2 Extended

At the middle in terms of precision is the Ultimaker 2 Extended. With the 0.4mm nozzle it has a layer resolution of **0.02mm**, which is more than double that of the Lulzbot Mini. One downside with the Ultimaker 2+ is that its baseplate has to be manually adjusted using the screws in each corner. Both Ultimaker printers we used use software, **Ultimaker Cura**.


  <div class="row pb-4">
    <div class="col">
      <img class="img-fluid py-1" src="../compare7.JPG" alt="compare7">
      The Ultimaker fared slightly better in the overhang test, as it could handle overhangs at an angle of up to <b>30 degrees</b> without any noticeable dripping. 
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../compare8.JPG" alt="compare8">
      At a right angle, basically any overhang had a <b>significant</b> amount of dripping plastic. 
    </div>
  </div>


#### Ultimaker S3

The Ultimaker S3 is the newest and most accurate **FDM printer(Fused Deposition Modeling)** in the Fablab. It has the same layer resolution as the 2+ Extended, but almost double the positioning precision. It also has **six sensors** for automatic adjustment of the baseplate. The biggest difference between the Ultimaker S3 and the two other printers is that the S3 can print **two different filaments simultaneously**. This can be used, for example, to print supports using a **water soluble PVA**. On the front-end side it has a nice touch screen that can be used to track the printing progress and to easily make adjustments.


  <div class="row">
    <div class="col">
      <img class="img-fluid py-1" src="../compare9.JPG" alt="compare9">
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../compare10.JPG" alt="compare10">
    </div>
    <p>In the bridging test it performed as well as the Ultimaker 2+, with no problems even at the largest gaps.</p>
  </div>
  

## Design Rule for the 3D printer

<img class="img-fluid pt-3 pb-1"  src="../rule1.png" alt="rule1">  

After importing the **.stl file**, we choosed **PLA**material. But for Ultimaker 2+ only have one option. Next we should adjust the model in to few centermeter size for printing faster, and then decided appropriate resollution.  
<img class="img-fluid pt-3 pb-1" style="width: 25rem;" src="../rule3.png" alt="rule3">
<img class="img-fluid pt-3 pb-1" style="width: 25rem;" src="../rule4.png" alt="rule4">  
We used **skirt** as the build plate adhesion type because it’s the easiest one to remove after printing. Certain prints require **brim** to remain **non-shrunk** model during printing, which I will show in the indiviual assignment.  
Here we have chosen **20%** as infill density and **Grid** as pattern as this gives us a good balance between durability, weight and print time.

  <div class="row">
    <div class="col">
      <img class="img-fluid py-1" src="../rule5.JPG" alt="rule5">
      Infill test by Ultimaker 2+ in different percentage.
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../rule6.JPG" alt="rule6">
      Shrunk model by <b>Skirt</b> build plate adhesion.
    </div>
  </div>

<img class="img-fluid pt-3 pb-1"  src="../rule2.png" alt="rule2">

Our model don't need any **support**, however, some models would be shown as **red**, which means it is possible to drip owning to gravity, then we need check the **support box** to generate support autometically. When everything set properlly, we click **slice** on the right bottom. The software will calculate the path of printer in the **[.gcode files](UM2E_model.gcode)**.


  <div class="row">
    <div class="col">
      <video class="img-fluid" controls width="800">
      	<source src="../rule7.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col">
      <video class="img-fluid" controls width="800">
      	<source src="../rule8.mp4" type="video/mp4">
      </video>
    </div>
    <p>Using the slider in Layer view we can see the structure of how each layer will be printed and how our Grid infill at 20% density will look.</p>
  </div>
  


<img class="img-fluid py-3" style="width: 30rem;" src="../rule9.JPG" alt="rule9">


  <div class="row">
    <div class="col">
      <video class="img-fluid" controls width="800">
      	<source src="../rule10.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col">
      <video class="img-fluid" controls width="800">
      	<source src="../rule11.mp4" type="video/mp4">
      </video>
    </div>
    <p>After series of settings, we finally can transfer the file into a 3D printer and press <b>PRINT</b>. What we need to do now is be patient :). The printing procedure is usually <b>several hours</b> (depend on the size), so we need to estimate the time when we build our project with it.</p>
  </div>




Then we got it!  
<img class="img-fluid py-3" src="../rule12.JPG" alt="rule12">

***


## Individual Assignment

#### Inspiration for the Structures of Final Project

<img class="img-fluid pt-3" src="../explore7.jpg" alt="explore7">

Helsinki Central Library Oodi

On the weekend, I visited **Oodi Helsinki City Library**. The building was designed in continuous lines, wood part seems like boat and the top seems like sea wave. It remind me the **Elbphilharmonie** in Hamburg, which has similar crown, but only Oodi was designed in parametric way.  

<img class="img-fluid pt-3" src="../explore8.jpg" alt="explore8">  

Elbphilharmonie, Hamberg

Therefore, I picked up parametric design skill I learned before. With **Grasshopper** I want to build something in round shape to imply the comfort psychologically, so I found the [tutorial](https://www.youtube.com/watch?v=dA63ia5i3eI&t=603s) and follow it.

Here is the program I build in the Grasshopper.
<img class="img-fluid py-3" src="../explore1.png" alt="explore1">
In the last step, I need a plug-in **[LunchBox](https://www.food4rhino.com/app/lunchbox)** for Grasshopper, to do a hexagon surface. After adjusting the surface, we can bake it.  
<img class="img-fluid py-3" src="../explore2.jpg" alt="explore2">

I exported it into the **.stl file** which can be understood by 3D printer software.
<img class="img-fluid py-3" src="../explore3.png" alt="explore3">

### ! Print

For this model, I printed both with **Ultimaker S3 and Formlabs Form 2** printers for experiment.

#### Ultimaker S3 

The setting is similar to group assignment.  
<img class="img-fluid py-3" style="width: 50rem;" src="../explore4.png" alt="explore4">

  <div class="row">
    <div class="col">
      <img class="img-fluid py-2" src="../explore5.JPG" alt="explore5">
    </div>
    <div class="col">
      <img class="img-fluid py-2" src="../explore6.JPG" alt="explore6">
    </div>
  </div>
  <div class="row">
    <div class="col">
      <img class="img-fluid py-2" src="../explore9.jpg" alt="explore9">
    </div>
    <div class="col">
      <img class="img-fluid py-2" src="../explore10.jpg" alt="explore10">
    </div>
    <p>Because the structrue was reduced to 10% of the original file, the structure was <b>thiner</b> than before. From the result, the top of structure seems was'nt printed really well, I am worry about after resolve the support, the structure will dimolish.</p>
  </div>

<video class="img-fluid pt-3 pb-1" controls width="800">
	<source src="../explore11.mp4" type="video/mp4">
</video>

The soluble support **PVA** can be resolved in the normal water, however with warm water can speed up the resovling procedure.

<img class="img-fluid pb-1" style="width: 50rem;" src="../explore12.JPG" alt="explore12">

Last glance before sleep.
<div class="row">
    <div class="col">
      <img class="img-fluid py-1 pb-3" src="../explore13.JPG" alt="explore13">
    </div>
    <div class="col">
      <img class="img-fluid py-1" src="../explore14.jpg" alt="explore14">
      After I woke up, the structure are totally disappear, only few thicker part left. @_@ I need to print it again... Let's see the result from <b>Fromlab Form2</b>.
    </div>
</div>

#### Fromlab Form2

From2 use its own sofaware, **PreFrom**, as model setting and editting. To decrease the printing time and save the support material, I reversed the model.  

<div class="row pb-4">
    <div class="col">
      <img class="img-fluid pb-1" src="../form3.JPG" alt="form3"> 
      <b>Form 2</b>
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../form4.JPG" alt="form4">
      User Interface, don't neglect the <b>instruction</b>
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../form5.JPG" alt="form5">
      Resin sink
    </div>
</div>

<img class="img-fluid pb-1" style="width: 70rem;" src="../form1.png" alt="form1">  

The **red part** was automaticly detected possible to drop because of an unsufficient support. To fix it, we need to add the support points manually.  
<img class="img-fluid py-3" style="width: 70rem;" src="../form2.png" alt="form2">

<div class="row pb-4">
    <div class="col">
      <img class="img-fluid pb-1" src="../form6.JPG" alt="form6"> 
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../form7.JPG" alt="form7">
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../form8.JPG" alt="form8">
    </div>
    <p>The printed model need to be washed by <b>alcohol</b> to remove the addtional resin. <b>Wearing gloves</b> in whole procedure is necessary to protect our hand.</p>
</div>
<img class="img-fluid " style="width: 70rem;" src="../form9.JPG" alt="form9">

**Alcoho sinks** are served for removing unecessary resin. Put the model in each for around 5 min.

<img class="img-fluid " style="width: 30rem;" src="../form10.JPG" alt="form10">

To stablize the model, it needs to be shined by **UV light** until the material no more stinky.

<img class="img-fluid " style="width: 30rem;" src="../form11.JPG" alt="form11">

All the support need to be cutted manually, this part took me most of the time. But I got a really impressive result!

<img class="img-fluid pb-4" src="../form12.JPG" alt="form12">


### 3D Scan

We have three 3D-scannig devices in Fablab, Artect3D scans with up to 16 FPS high frequency light to capture small to medium-sized objects. Asus Xtion uses infrared sensor to capture objects.


<div class="row pb-4">
    <div class="col">
      <img class="img-fluid pb-1" src="../scan1.JPG" alt="scan1"> <b>Artec3D Eva</b>
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../scan2.JPG" alt="scan2"><b>ASUS XTION + SKANECT PRO</b>
    </div>
</div> 

<div class="row pb-4">
    <div class="col">
      <img class="img-fluid pb-1" src="../scan3.JPG" alt="scan3">I tried to use <b>Artec3D</b> to scan grapes and a mug, however because of comlicated appreance of grapes and continuous appreance of mug. It didn't work really well. The charging part was so loose that the software couldn't connect it, so I switch to <b>Asus Xtion</b>.
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../scan4.JPG" alt="scan4">
    </div>
</div> 

<img class="img-fluid "  src="../scan5.png" alt="scan5">

It is relatively easier to scan **an irregular object**, for example a human.

<img class="img-fluid "  src="../scan6.png" alt="scan6">

After scanning we can also edit the file in software. For example to fill up some holes or cut off some broken part. 

<img class="img-fluid "  src="../scan7.png" alt="scan7">

We can export it into **.stl file** to compatible file by 3D printer. The printing result can be seen in the group assignment. :)

**Hero shot**
<img class="img-fluid pb-4"  src="../scan8.jpg" alt="scan8">


### Design file

<img class="img-fluid pb-4"  src="../design1.jpg" alt="design1">

- [3D file](../structure-final.stl)
- [Grasshopper file](../structure.gh)
- [3D scan model](../model.stl)

