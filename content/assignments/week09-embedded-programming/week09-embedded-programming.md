+++
title ="Week 09: Embedded Programming"
+++

<img class="img-fluid pb-3" src="../present.png" alt="present">


## Assignment

- Individual assignment:
  - Read a microcontroller data sheet
  - Program your board to do something, with as many different programming languages and programming environments as possible
- Group assignment:
  - Compare the performance and development workflows for other architectures

## Individual Assignment

### megaTinyCore

To recognize ATtiny412 board, we need to install **megaTinyCore** by following the instruction from [Spence Konde](https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md).

This board package can be installed via the board manager. The boards manager URL is:

```
http://drazzy.com/package_drazzy.com_index.json
```

1. File -> Preferences, enter the above URL in "Additional Boards Manager URLs"
2. Tools -> Boards -> Boards Manager...
3. Wait while the list loads (takes longer than one would expect, and refreshes several times).
4. Select "megaTinyCore by Spence Konde" and click "Install". For best results, choose the most recent version.

<img class="img-fluid pb-3" style="width: 50rem;" src="../mega1.png" alt="">

After finishing the setting, we can start to program our board with the **UPDI board** which I built in **Week 05 Electronics Production**. Use the code from [echo.ino](http://academy.cba.mit.edu/classes/embedded_programming/t412/hello.t412.echo.ino).

```
#define max_buffer 25

static int index = 0;
static char chr;
static char buffer[max_buffer] = {0};

void setup() {
   //Serial.swap(1);
   Serial.begin(115200);
   }

void loop() {
   if (Serial.available() > 0) {
      chr = Serial.read();
      Serial.print("hello.t412.echo: you typed \"");
      buffer[index++] = chr;
      if (index == (max_buffer-1))
         index = 0;
      Serial.print(buffer);
      Serial.println("\"");
      }
   }
```

We can get the echo successfully. And we don't want to **swap** the pin here, thus we blocked it out.  
<img class="img-fluid py-3" style="width: 50rem;" src="../mega2.png" alt="">
<img class="img-fluid pb-3" style="width: 50rem;" src="../mega3.png" alt="">

Another try with [echo.c](http://academy.cba.mit.edu/classes/embedded_programming/t412/hello.t412.echo.c). Here is **key point**.  
For using the code we need to change the define of **serial_pin_out** and **serial_pin_in**. Refer to <span style="color: green">PA6</span> and <span style="color: green">PA7</span>, we used **PIN6_bm and PIN67_bm** instead. The reference could be found from [here](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/variants/txy6/pins_arduino.h).  



<img class="img-fluid py-3" style="width: 50rem;" src="../../week07-electronics-design/prepare3.gif" alt="">

<img class="img-fluid py-3" style="width: 30rem;" src="../mega6.png" alt="">

<img class="img-fluid py-3" style="width: 50rem;" src="../mega4.png" alt="">
<img class="img-fluid pb-3" style="width: 50rem;" src="../mega5.png" alt="">

### ?What I learn from microcontrollwe Datasheet (ATtiny414)


I read the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny212-412-DataSheet-DS40001911B.pdf) of **ATtiny412**. And check the pin information from 
[SpenceKonde](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md). The following points are what I learned, and the further description could be found from [Week12: Input Devices](../../week12-input-devices/week12-input-devices).

<div class="row pb-3">
    <div class="col-3">
      <img class="img-fluid" src="../../week12-input-devices/my8.png" alt="">
    </div>
    <div class="col-9">
      <img class="img-fluid" src="../../week12-input-devices/my5.gif" alt="">
    </div>
</div>



#### Bypass Capacitor (Decoupling Capacitors)


<img class="img-fluid" style="width: 30rem;" src="../../week12-input-devices/my7.png" alt="">   

(picture from Eagle Academy)


#### !Analog-to-Digital Converter (ADC)

<img class="img-fluid" style="width: 30rem;" src="../../week12-input-devices/adc1.jpg" alt="">  


<img class="img-fluid pb-3" style="width: 30rem;" src="../../week12-input-devices/adc2.jpg" alt="">  


#### ?Vcc vs Vdd

In the datesheet and a lot of schematic we can see **Vcc** and **Vdd** almost everywhere. I was so confused what the different in between. The answer from [What is the difference between Vcc, Vdd, Vee, Vss?](https://electronics.stackexchange.com/questions/17382/what-is-the-difference-between-v-cc-v-dd-v-ee-v-ss) explains that "In practise today **Vcc (collector or commom collerctor voltage) and Vdd (drain) mean the positive voltage (+)**, and **Vee (emitter) and Vss (source) mean the ground (-)."**

#### ?Direction of LED

<img class="img-fluid py-3" src="../../week12-input-devices/my17.png" alt="">

### Program my own code

This part was finished by [Week12: Input Devices](../../week12-input-devices/week12-input-devices).
```
//tx_rx03  Robert Hart Mar 2019.
//https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/index.html
//Step Response TX, RX by Adrián Torres Omaña
//Fab Academy 2021
//Step Response TX, RX
//Adrianino
//https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#step

//Modified by Russian Wu
//Fab Academy 2021
//Step Response TX, RX

//  Program to use transmit-receive across space between two conductors.
//  One conductor attached to digital pin, another to analog pin.
//
//  This program has a function "tx_rx() which returns the value in a long integer.
//
//  Optionally, two resistors (1 MOhm or greater) can be placed between 5V and GND, with
//  the signal connected between them so that the steady-state voltage is 2.5 Volts.
//
//  Signal varies with electric field coupling between conductors, and can
//  be used to measure many things related to position, overlap, and intervening material
//  between the two conductors.
//


long result;   //variable for the result of the tx_rx measurement.
int analog_pin = 2; //  PA1 of the ATtiny412
int tx_pin = 3;  //     PA2 of the ATtiny412


void setup() {
pinMode(tx_pin,OUTPUT);      //Pin 2 provides the voltage step
Serial.begin(115200);
}


long tx_rx(){         //Function to execute rx_tx algorithm and return a value
                      //that depends on coupling of two electrodes.
                      //Value returned is a long integer.
  int read_high;
  int read_low;
  int diff;
  long int sum;
  int N_samples = 100;    //Number of samples to take.  Larger number slows it down, but reduces scatter.

  sum = 0;

  for (int i = 0; i < N_samples; i++){
   digitalWrite(tx_pin,HIGH);              //Step the voltage high on conductor 1.
   read_high = analogRead(analog_pin);        //Measure response of conductor 2.
   delayMicroseconds(100);            //Delay to reach steady state.
   digitalWrite(tx_pin,LOW);               //Step the voltage to zero on conductor 1.
   read_low = analogRead(analog_pin);         //Measure response of conductor 2.
   diff = read_high - read_low;       //desired answer is the difference between high and low.
 sum += diff;                       //Sums up N_samples of these measurements.
 }
  return sum;
}                         //End of tx_rx function.


void loop() {

result = tx_rx();
result = map(result, 6000, 11000, 0, 1024);  //I recommend mapping the values of the two copper plates, it will depend on their size
Serial.println(result);
delay(100);
}
```

### ESP32-CAM

I tried to use **ESP32-CAM** to get some videos.

<img class="img-fluid pb-3" style="width: 30rem;" src="../esp1.JPG" alt="">

First, I found a [tutorial](https://youtu.be/visj0KE5VtY) for having a brief introduction. Later for connecting **FTDI cable and ESP32-CAM** correctly, I found the datasheet of them.  
ESP32-CAM [Darasheet](https://media.digikey.com/pdf/Data%20Sheets/DFRobot%20PDFs/DFR0602_Web.pdf)  
<img class="img-fluid pt-3" style="width: 50rem;" src="../esp2.png" alt="">

<img class="img-fluid pb-3" style="width: 30rem;" src="../esp3.jpg" alt="">

Connect them with a breadboard and some cables.  
<img class="img-fluid pt-3" style="width: 40rem;" src="../esp4.png" alt="">

<img class="img-fluid pb-3" style="width: 40rem;" src="../esp5.JPG" alt="">

Install the esp32 from Boards Manager, I learned from this [website](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/).  
<img class="img-fluid pt-3" style="width: 40rem;" src="../esp6.png" alt="">
<img class="img-fluid py-3" src="../esp7.png" alt="">

Open CameraWebServer examlpe for testing.  
<img class="img-fluid pt-3" src="../esp8.png" alt="">

I met a **challenge** that the program could be uploaded to the **ESP-32 CAM**.
<img class="img-fluid pt-3" src="../esp9.png" alt="">

So I googled for the solution. There is several solution and potential answers.

[Github](https://github.com/espressif/arduino-esp32/issues/333)
[Tutorials](https://randomnerdtutorials.com/program-upload-code-esp32-cam/)

In conclusion, 
1. I hold the **RESET** button while **Connecting.....*
2. Change into a **shorter USB cable**.
3. Install a lasted driver from [VCP Drivers](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers)
4. Modify the **Flash Frequency: 40MHz** and **Flash Mode: into DIO**
5. Change **3.3V VCC into 5V**
5. Change **Programmer: AVRISP mkll**  
<img class="img-fluid pt-3" style="width: 30rem;" src="../esp10.png" alt="">

And it worked!  
<img class="img-fluid pt-3" src="../esp11.png" alt="">
<img class="img-fluid pt-3" src="../esp12.png" alt="">

But the wired thing is I couldn't repeat the uploading workflow in the same setting condition. I found a comment that they said it might because of the bad quality of **USB cable or ESP32 itself**.


***

## Group Assignment
### Comparing ESP32-CAM, Aruino UNO REV3 and NodeMCU - ESP8266

<div class="row pb-3">
    <div class="col">
    	<b>ESP32-CAM</b>
      <img class="img-fluid pt-3" src="../esp1.JPG" alt="present">
    </div>
    <div class="col">
      <b>Aruino UNO REV3</b>
      <img class="img-fluid pt-3" src="../group2.JPG" alt="present">
    </div>
    <div class="col">
    	<b>NodeMCU - ESP8266</b>
      <img class="img-fluid pt-3" src="../group1.JPG" alt="">
    </div>
</div>

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pt-3" src="../esp2.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pt-3" src="../group3.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pt-3" src="../group4.png" alt="">
    </div>
</div>


<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">ESP32-CAM</th>
      <th scope="col">Aruino UNO REV3</th>
      <th scope="col">NodeMCU - ESP8266</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Microcontroller</th>
      <td></td>
      <td>ATmega328P</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Operating Voltage</th>
      <td>3.3V or 5V</td>
      <td>5V</td>
      <td>3.3V</td>
    </tr>
    <tr>
      <th scope="row">Flash Memory</th>
      <td>SPI Flash 32Mbit</td>
      <td>32 KB</td>
      <td>4MB</td>
    </tr>
    <tr>
      <th scope="row">RAM</th>
      <td>built-in 520 KB+external 4MPSRAM</td>
      <td>SRAM 2 KB</td>
      <td>SRAM 64 KB</td>
    </tr>
    <tr>
      <th scope="row">IO port</th>
      <td>9</td>
      <td>Digital 14(6 provide PWM output) / Analog Input 6</td>
      <td>Digital 16 / Analog 1</td>
    </tr>
    <tr>
      <th scope="row">Serial Port Baud-rate</th>
      <td>Default 115200 bps</td>
      <td></td>
      <td>Default 9600 bps</td>
    </tr>
    <tr>
      <th scope="row">Wi-Fi</th>
      <td>802.11b/g/n/e/i</td>
      <td></td>
      <td>802.11b/g/n</td>
    </tr>
    <tr>
      <th scope="row">Bluetooth</th>
      <td> Bluetooth 4.2 BR/EDR and BLE standards</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Datasheet</th>
      <td><a href="https://media.digikey.com/pdf/Data%20Sheets/DFRobot%20PDFs/DFR0602_Web.pdf">DFRobot</a></td>
      <td><a href="https://store.arduino.cc/arduino-uno-rev3">Arduino Website</a></td>
      <td><a href="https://components101.com/development-boards/nodemcu-esp8266-pinout-features-and-datasheet">Components</a></td>
    </tr>
  </tbody>
</table>




## Download

- [echo.c](../echo.c)
- [echo.ino](../echo.ino)

