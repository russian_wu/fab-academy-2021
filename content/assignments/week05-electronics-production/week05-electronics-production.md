+++
title ="Week 05: Electronics Production"
+++
<img class="img-fluid pb-3" src="../present.jpg" alt="present">


## Assignment
- Group Assignment:
	- characterize the design rules for your PCB production process
- Individual Assignment:
	- make an **in-circuit programmer** by milling and stuffing the PCB, test it, then optionally try other PCB processes


## Group Assignment

- Group Members: Daniel Wilenius, Dann Mensah, 吳若玄 (Russian Wu)

### Milling Machine - SRM-20
<img class="img-fluid py-3" style="width: 60rem;" src="../milling1.JPG" alt="milling1">

### Equipments we need:
1. one-sided Copper board  
<img class="img-fluid py-3" style="width: 30rem;" src="../milling2.JPG" alt="milling2">
2. 0.3, 0.4mm, and 0.8mm **milling bits**  
<img class="img-fluid py-3" style="width: 30rem;" src="../milling3.JPG" alt="milling3">
3. Linetest pictures, the **white parts** are going to be kept.  
<img class="img-fluid py-3" style="width: 20rem;" src="../milling4.png" alt="milling4">
<img class="img-fluid py-3" style="width: 20rem;" src="../milling5.png" alt="milling5">

### Milling Machine (SRM-20) setting up

Set the origin of z axis first and then x/y axis.  
<img class="img-fluid py-3" style="width: 40rem;" src="../milling6.png" alt="milling6">
<img class="img-fluid py-3" style="width: 30rem;" src="../milling7.JPG" alt="milling7">

### Create the milling path for PCB (Printed Copper Board)

Use the [mods](http://mods.cba.mit.edu/) to create the .rml files. The parameters are important. Once import the png file, select the tool diameter (depends on the milling bits we use). Cut depth and maximum depth suppose to be the **thickness of copper**, so we set it into 0.1. Usually **0.4mm for the interial content and 0.8 mm for the outline** to cut the PCB off. 0.4 mm milling bits should use offset number 4 and 0.3 mm one could use offset number 5-6.    
<img class="img-fluid py-3" style="width: 60rem;" src="../milling8.png" alt="milling8">  
When we cut the outline, the tool diameter change to 0.8 bit, and the cut depth should **less than tool diameter** and 0.6 is 75% of the bit, which is perfect. The thickness of coppered board is 1.8 so that is the maximun depth. Once it's all set well, then click the calculate button.  
<img class="img-fluid py-3" style="width: 60rem;" src="../milling9.png" alt="milling9">  
The speeds of each size of milling bits need to follow the instruction, otherwises the bits would be broken. The percentage of the speed should always start from **20% to 100%** in the manual tab.  
<img class="img-fluid py-3" style="width: 20rem;" src="../milling10.jpg" alt="milling10">
<img class="img-fluid py-3" style="width: 40rem;" src="../milling11.png" alt="milling11">

### ! Start to cut

After setting all the parameters carefully, just click the cut button bravely!  
<video class="img-fluid py-3" controls width="800">
	<source src="../cut1.mp4" type="video/mp4">
</video>

These are what we got after experimenting with 0.3mm and 0.4mm milling bits, the smaller one obiviouslly can do a more accurate milling. 
<img class="img-fluid py-3" style="width: 30rem;" src="../cut2.JPG" alt="cut2">

## !? Mistakes
While I was milling the group assignment with a 0.4mm bit, it was broken. After the double check with all parameters, it broke second times. :(  
<img class="img-fluid py-3" style="width: 30rem;" src="../mistake1.JPG" alt="mistake1">
<img class="img-fluid py-3" style="width: 50rem;" src="../mistake2.JPG" alt="mistake1">  
After the discussion with Kris, the problem might because of the uneven base that leads to the bit endures with too much horizontal force. So we relocated the copper board, afterwards the following steps were smoother.

***

## Individual Assignment

To make an **in-circuit programmer**, I did basicly the same procedure with the group assignment. But I did a small modification to the outline of USB adapter. By the experience from last year, the adapter needs to be longer to insert into USB port.  
<img class="img-fluid py-3" style="width: 9rem;" src="../usb1.png" alt="usb1">
<img class="img-fluid py-3" style="width: 9rem;" src="../usb3.png" alt="usb3"> → 
<img class="img-fluid py-3" style="width: 9rem;" src="../usb2.png" alt="usb2">  
Import the png files into [mods](http://mods.cba.mit.edu/) and change the parameters to caculate the paths for milling machine. Instead of 0.4mm bit I used **0.3mm** one this time.  
<img class="img-fluid py-3" style="width: 70rem;" src="../usb4.png" alt="usb4">
<img class="img-fluid py-3" style="width: 70rem;" src="../usb5.png" alt="usb5">  
Time to milling!  
<video class="img-fluid py-3" controls width="400">
	<source src="../usb6.mp4" type="video/mp4">
</video>  
And I successfull got this!  
<img class="img-fluid py-3" style="width: 30rem;" src="../usb7.JPG" alt="usb7">  

## Soldering (Stuffing)

Our goal is to do the samething from the [Neil's example](http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.jpg).  
<img class="img-fluid py-3" style="width: 15rem;" src="../soldering1.jpg" alt="soldering1">  
1. Collect the resistors, capacities, and connector  
	<img class="img-fluid py-3" style="width: 30rem;" src="../soldering2.JPG" alt="soldering2">
2. Do it properly under the microscope, and piece by piece  
	<img class="img-fluid py-3" style="width: 50rem;" src="../soldering3.JPG" alt="soldering3">
3. Ta da!  
	<img class="img-fluid py-3" style="width: 30rem;" src="../soldering4.JPG" alt="soldering4">

## ! Let's test
To prevent the shortcut destories the port of laptop, it's good to use a Usb adapter.  
<img class="img-fluid py-3" style="width: 40rem;" src="../test3.JPG" alt="test3">
- First test on the Linux.  
<img class="img-fluid py-3" style="width: 50rem;" src="../test1.png" alt="test1">
- Second test on the MaxOS, from the result we knew that the usb serial number is **D30725LF**.  
<img class="img-fluid py-3" style="width: 50rem;" src="../test2.png" alt="test2">

## !? Challenges
Besides broke the milling bits in the group assignments, I tried with different offset number in the setting. However, the 0.4mm milling bits is too thick to remove all the copper. **Offset number: 1 and 4**. Consiquently, I used **a 0.3mm bit and 5 offset** in the end.  
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge1.jpg" alt="challenge1">
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge2.JPG" alt="challenge2">  

While we were testing the PCB Usb adapter, the connector was accidently broken. So we decided to use the **driller** from the wood workshop to make the connected points more steady.  
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge3.JPG" alt="challenge3">
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge4.JPG" alt="challenge4">  

### Emergency solved!  
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge5.JPG" alt="challenge5">
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge6.JPG" alt="challenge6">
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge7.JPG" alt="challenge7">


