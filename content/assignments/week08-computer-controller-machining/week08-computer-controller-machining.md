+++
title ="Week 08: Computer-Controlled Machining"
+++

<div class="row">
    <div class="col-4">
      <img class="img-fluid pb-3" src="../present1.jpg" alt="present">
    </div>
    <div class="col-8">
      <img class="img-fluid pb-3" src="../present.png" alt="present">
    </div>
</div>

<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../present2.JPG" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../present3.jpg" alt="present">
    </div>
</div>

## Assignment

- Group assignment:
  - do your lab's safety training
  - test runout, alignment, speeds, feeds, materials, and toolpaths for your machine
- Individual assignment:
  - make (design+mill+assemble) something big (meter-scale)

## Individual Assignment

In our dormitary, we need a shoe rack for our boots and shoes. So I found [this tutorial](https://youtu.be/VJWz-exP4iY) to lead me to create a shoe rack. To design a proper size of it, I also refered the product from [IKEA](https://www.ikea.com/gb/en/search/products/?q=shoe%20rack).
I create a working flow for this week.

1. Measurement and requirement
2. Learning from tutorials
3. Design my own shoe rack
4. Documentation

#### Measurement 

Base on the measurement of our entrance hall, I had the following parameter.

<img class="img-fluid pb-3" style="width: 40rem;" src="../measure1.JPG" alt="">
<img class="img-fluid pb-3" style="width: 40rem;" src="../measure2.JPG" alt="">

So I entered all the **parameters** into Fusion 360.

<img class="img-fluid pb-3" src="../measure3.png" alt="">

#### Learning from tutorials

Here is the list that I learned from:
1. How to design a shoe rack? [Here](https://youtu.be/VJWz-exP4iY)
2. How to **auto** arrange the design on sheet? [Nest](https://youtu.be/TIBMX-oVasU)
3. How to create toolpath? [Last Fab Academy](https://youtu.be/Xb4e8mysC24)

#### Design my own shoe rack

I started the design from the side and extrude it with the parameters I set.  
<img class="img-fluid py-3" style="width: 40rem;" src="../design1.jpg" alt="">

Cut the joint for the beams.  
<img class="img-fluid py-3" style="width: 40rem;" src="../design2.jpg" alt="">

Copy the joints and arrange them according to my design.  
<img class="img-fluid py-3" style="width: 40rem;" src="../design3.jpg" alt="">

Create a beam with **press-fit** joint, mirror it, and extrude it. Save the beam as **an external file**, which make us easier to update it afterward.  

<img class="img-fluid py-3" style="width: 40rem;" src="../design4.jpg" alt="">
<img class="img-fluid py-3" style="width: 40rem;" src="../design5.jpg" alt="">

Copy another side of shoe rack.  
<img class="img-fluid py-3" style="width: 40rem;" src="../design6.jpg" alt="">

With **[Dogbone plug-in](https://github.com/DVE2000/Dogbone)**, it helps us to create dogbone easier.  
<img class="img-fluid" style="width: 40rem;" src="../design7.jpg" alt="">
<img class="img-fluid" style="width: 40rem;" src="../design8.jpg" alt="">

Arrange the componets on the sheets with new functions **[Nest](https://youtu.be/TIBMX-oVasU)** automatically.  
<img class="img-fluid" style="width: 40rem;" src="../design9.jpg" alt="">
<img class="img-fluid pb-3" style="width: 40rem;" src="../design10.png" alt="">

#### ! Create Toolpath

1. Set Up. Select the **bottom of material** is a critcal point for locating the **origin point** of the CNC machine.
<img class="img-fluid py-3" src="../tool1.png" alt="">
<img class="img-fluid py-3" style="width: 20rem;" src="../tool2.png" alt="">

2. 2D Contour  
Name the tool.  
<img class="img-fluid py-3" style="width: 60rem;" src="../tool3.png" alt="">   
According to the measurement, key in the dimensions of the bit.  
<img class="img-fluid py-3" style="width: 60rem;" src="../tool4.png" alt="">  
<span style="color: red">!!! The parameters at here is important and directly relate to the cutting result and if the bit works properly.</span> Please refer to the following sheet that could be found in <span style="color: green">green handbook</span> besides the CNC room. **Feed / revolution** should be **half** of **feed / tooth (chip load)**.
<img class="img-fluid py-3" style="width: 60rem;" src="../tool5.png" alt="">
<img class="img-fluid py-3" style="width: 40rem;" src="../tool13.png" alt="">  
Depends on different materials and diameters of milling tools, we need to use different parameters. For example, I used a **4mm** bit and material close to softwood. Therefore, I took **0.25** as my chip load. Although Fusion 360 will caculate feedrate for us automatically, but we also need know the formular. **Feedrate = flutes * chip load * spindle speed**.  
<img class="img-fluid py-3" style="width: 40rem;" src="../tool6.JPG" alt="">  
Select all the outlines that need to be cutted. Create **taps** to stabilize the materials.
<img class="img-fluid py-3" style="width: 60rem;" src="../tool7.png" alt="">  
**Maximum Stepdown** should be half of the bit size. Axial stock set to **-0.5mm** to ensure that all the materials are cutted through.  
<img class="img-fluid py-3" style="width: 20rem;" src="../tool8.png" alt="">  
After create the path, we can simulate the cutting to cofirm the cut performance.   
<img class="img-fluid py-3" style="width: 20rem;" src="../tool9.png" alt="">
<img class="img-fluid py-3" style="width: 20rem;" src="../tool10.png" alt="">  
Click **Post Process** to export the toolpath.  
<img class="img-fluid py-3" style="width: 20rem;" src="../tool11.png" alt="">
<img class="img-fluid py-3" style="width: 20rem;" src="../tool12.png" alt="">

<span style="color: red"><b>!Tips:</b></span> Cut the pockets first, then cut the outlines. The material would be **more stable**.

#### Cut it in CNC machine

This week we cut our works with CNC machine, in the following context I will explain how to use the giant machine safely and properly.  
<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc1.JPG" alt="">  

Choose the **bit size and the head of it** according to my need.  
<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc2.JPG" alt="">  

Install the **drilling chuck** properly and push it into the **spindel** until hearing a **clicking sound**. Make sure during the installation the **spounge** under the bit to protect it. And tight the spindle up with **wernchs**.  
<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc3.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc7.JPG" alt="">
    </div>
</div>
<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc4.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc5.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc6.JPG" alt="">
    </div>
</div>

Before turning on the vaccum, unplug the **valves** and lay out the robber bands properly to ensure the vaccum can suck our material tightly. And the barometer should point to <span style="color: green">green zone</span>.

<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc8.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc9.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc10.JPG" alt="">
    </div>
</div>

By following the instruction on the wall of CNC room, we can set up the cutting properly.  
<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc11.JPG" alt="">
<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc12.JPG" alt="">  

Set the origin of **Z-axis** while the **vaccum turn on**.
<div class="row">
    <div class="col">
      <video class="img-fluid pb-2" controls width="800">
        <source src="../cnc13.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col">
      <video class="img-fluid pb-2" controls width="800">
        <source src="../cnc14.mp4" type="video/mp4">
      </video>
    </div>
</div>

Set the origin of **X and Y axis** with **↑↓←→, PgUp and Pg Dn keys**. 

<video class="img-fluid pb-2" controls width="800">
	<source src="../cnc15.mp4" type="video/mp4">
</video>

After finishing all the setting, we need to leave the room and close the door to operate the rest task outside. Upload the **gcode** in the **Mach3 software** and press the green button to start the work.

<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc16.JPG" alt="">  

<div class="row pb-3">
    <div class="col">
      <video class="img-fluid pt-2" controls width="800">
        <source src="../cnc17.mp4" type="video/mp4">
      </video>
      <span>10x speed</span>
    </div>
    <div class="col">
      <video class="img-fluid pt-2" controls width="800">
        <source src="../cnc18.mp4" type="video/mp4">
      </video>
      <span>5x speed</span>
    </div>
</div>

Then our cut is done!  
<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc19.JPG" alt=""> 

To cut off the tabs, **a hammer and a chisel** can help a lot.  
<img class="img-fluid pb-3" style="width: 30rem;" src="../cnc20.JPG" alt="">

The bit on the spindle need to be removed and the space need to be cleaned with **a vaccum or an air gun**.  
<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc21.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cnc22.JPG" alt="">
    </div>
</div>

#### Cutting Result

##### Outlines.
<div class="row">
    <div class="col-5">
      <img class="img-fluid" src="../result1.JPG" alt="">
      <span>20mm <span style="color: red">+ 0.17mm (Inaccuracy)</span></span>
    </div>
    <div class="col-7">
      <img class="img-fluid" src="../result5.JPG" alt="">
      <span>58mm</span>
    </div>
</div>

##### Pockets.
<div class="row pb-3">
	<div class="col">
      <img class="img-fluid" src="../result3.JPG" alt="">
      <span>10mm <span style="color: red"> - 0.04mm (Inaccuracy)</span></span>
    </div>
    <div class="col">
      <img class="img-fluid" src="../result4.JPG" alt="">
      <span>18mm <span style="color: red"> + 0.18mm (Inaccuracy)</span></span>
    </div>
    <div class="col">
      <img class="img-fluid" src="../result2.JPG" alt="">
      <span>10mm <span style="color: red">+ 0.06mm (Inaccuracy)</span></span>
    </div>
</div>

According to the measurement, I understood that while I cut with **4mm** bit.

<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col">4 mm bit</th>
      <th scope="col">Inaccuracy</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Outlines</th>
      <td>+0.085 mm</td>
    </tr>
    <tr>
      <th scope="row">Pockets</th>
      <td>+0.09 mm </td>
    </tr>
  </tbody>
</table>

Besides bigger dogbones, I also interested in the diameters of the cross beam that cutted with **6mm** bit.

<div class="row pb-3">
	<div class="col-5">
      <img class="img-fluid pt-3" src="../result6.JPG" alt="">
      <span>20mm <span style="color: red"> + 0.25mm (Inaccuracy)</span></span>
    </div>
    <div class="col-7">
      <img class="img-fluid pt-3" src="../result7.JPG" alt="">
      <span>18mm <span style="color: red"> + 0.14mm (Inaccuracy)</span></span>
    </div>
</div>

<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col">6 mm bit</th>
      <th scope="col">Inaccuracy</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Outlines</th>
      <td>+0.125 mm</td>
    </tr>
    <tr>
      <th scope="row">Pockets</th>
      <td>+0.07 mm </td>
    </tr>
  </tbody>
</table>

#### Post Processing

##### Sanding

<div class="row py-3">
	<div class="col">
      <img class="img-fluid" src="../sanding1.JPG" alt="">
      Sanding machine and sanding paper.
    </div>
    <div class="col">
      <img class="img-fluid" src="../sanding2.JPG" alt="">
      C470 sanding paper is <b>rough</b> one.
    </div>
</div>
<div class="row py-3">
	<div class="col">
      <img class="img-fluid pb-1" src="../sanding3.JPG" alt="">
      Originally the sides weren't smooth and the wood chips easily to stab into hands or fingers. The surface became more smooth and clean after sanding.
      <p><b style="color: red">!! Tips:</b> Sand firstly with rougher sanding papers then finer, it can improve the sanding efficiency.</p>
    </div>
    <div class="col">
      <img class="img-fluid" src="../sanding4.JPG" alt="">
    </div>
</div>

##### Fix the Shoe Rack with small hack

The shoe rack was unsteady becasue of some cutting inaccuracies and the tolerance I left for joints. Also, the thickness wasn't precise **18mm**. 

<img class="img-fluid" src="../fix1.jpg" alt=""> 

<div class="row py-3">
	<div class="col">
      <img class="img-fluid" src="../fix2.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../fix3.JPG" alt="">
    </div>
</div>

To fix the unstable problem, there are two solutions either create triangle blocks to each of joints or or stuck them with thin woods. I choosed later, the elegant and easy way, to fix it at the end. In order to fix it permanently, I also painted some white glue in between.

<div class="row pb-3">
	<div class="col">
      <img class="img-fluid" src="../fix4.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../fix5.JPG" alt="">
    </div>
</div>

<div class="row pb-3">
	<div class="col">
      <img class="img-fluid" src="../fix7.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../fix6.JPG" alt="">
    </div>
</div>

##### !! Fixed

<img class="img-fluid pb-3" style="width: 60rem;" src="../fix8.JPG" alt=""> 

##### Coating

<div class="row pb-3">
	<div class="col">
      <img class="img-fluid" src="../fix9.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../fix10.JPG" alt="">
    </div>
</div>

##### !! Done

<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../present2.JPG" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../present3.jpg" alt="present">
    </div>
</div>


## Challenges

1. The toolpath of dogbones couldn't be created.

<img class="img-fluid pb-3" style="width: 60rem;" src="../challenge1.png" alt="">  

**Solution:** According to several experiment, a <b>4mm bit need to + <span style="color: red">1mm</span></b> and a <b>6mm bit need to + <span style="color: red">2mm</span></b> to create toolpath inside dogbones successfully. It might be a bug of the dogbone plug-in.  
<img class="img-fluid py-3" style="width: 20rem;" src="../challenge2.png" alt="">

2. Because the arrangement of each component is **too close**. To avoid the cutting problem, such as flying component. Kris taught me to align the tabs to fix all of the beams manually.

<img class="img-fluid pb-3" src="../challenge9.png" alt="">

<img class="img-fluid pb-3" style="width: 20rem;" src="../challenge10.png" alt="">

3. The 4mm bit broke during the last of cutting procedure.
<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../challenge3.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../challenge4.JPG" alt="">
    </div>
</div>

**Assumption Reason:** The speed of herizontal moving might be too fast to give too much force on the herizontal side. So I decreased the **cutting feedrate** either by decreased **chip load(Feed per tooth)** or by percentage on the **control panel**.

**Solution:** Because we don't have others 4mm bits, I changed to 6mm to finish the rest of cutting. Fortunately, the rest of cut is only outlines, so 6mm bit could also accomplish the work for 4mm.

<img class="img-fluid pb-3" src="../challenge11.JPG" alt="">


4. The vaccum worked not really well, therefore the wood wasn't stable on the cutting table.

<img class="img-fluid pb-3" src="../challenge5.jpg" alt="">

In stead of the broken vaccum, at the end we used **clamps** to fix the wood.

<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../challenge8.jpg" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../challenge7.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../challenge6.JPG" alt="">
    </div>
</div>

## Design File

<img class="img-fluid pb-3" style="width: 40rem" src="../design6.jpg" alt="">

- [Shoe Rack.f3d](../ShoeRack.f3d)

- [Shoe Rack.obj](../final-design.obj)

- [Toolpath](../shoe-rack-4mm.tap)

***

## Group Assignment

<img class="img-fluid py-3" style="width: 30rem" src="../group.JPG" alt="">

The cutting procedure was recorded in my indiviual assignment.
<div class="row pb-3">
    <div class="col-8">
      <video class="img-fluid pt-2" controls width="800">
        <source src="../cut1.mp4" type="video/mp4">
      </video> 
      <p>5x Speed</p> 
    </div>
    <div class="col-4">
      <video class="img-fluid pt-2" controls width="800">
        <source src="../cut2.mp4" type="video/mp4">
      </video>
    </div>
</div>

<img class="img-fluid pb-3" src="../cut3.JPG" alt="">

**!! Tips to cut off tabs**

<video class="img-fluid pb-3" controls width="400">
	<source src="../group13.mp4" type="video/mp4">
</video>

### Measurement of cutting result

#### Outlines

<div class="row pb-3">
    <div class="col-5">
      <img class="img-fluid" src="../group1.JPG" alt="">
      <span>100mm <span style="color: red"> <b>+</b> 0.23mm (Inaccuracy)</span></span>
    </div>
    <div class="col-7">
      <img class="img-fluid" src="../group2.JPG" alt="">
      <span>150mm <span style="color: red"> <b>+</b> 0.29mm (Inaccuracy)</span></span>
    </div>
</div>

#### Pockets

<div class="row pb-3">
    <div class="col-5">
      <img class="img-fluid pb-3" src="../group3.JPG" alt="">
      <span>50mm <span style="color: red"> <b>-</b> 0.19mm (Inaccuracy)</span></span>
    </div>
    <div class="col-7">
      <img class="img-fluid pb-3" src="../group4.JPG" alt="">
      <span>75mm <span style="color: red"> <b>-</b> 0.20mm (Inaccuracy)</span></span>
    </div>
</div>

#### Summary

<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col">12 mm bit</th>
      <th scope="col">Inaccuracy</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Outlines</th>
      <td>+ 0.13 mm = ((0.23+0.29)/2)/2</td>
    </tr>
    <tr>
      <th scope="row">Pockets</th>
      <td>- 0.10 mm = (-0.20)/2</td>
    </tr>
  </tbody>
</table>

### Another try

<img class="img-fluid py-3" style="width: 40rem" src="../group5.JPG" alt="">

#### Outlines

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../group6.JPG" alt="">
      <span>100mm <span style="color: red"> <b>+</b> 0.11mm (Inaccuracy)</span></span>
    </div>
    <div class="col">
      <img class="img-fluid" src="../group7.JPG" alt="">
      <span>100mm <span style="color: red"> <b>+</b> 0.11mm (Inaccuracy)</span></span>
    </div>
</div>

#### Pockets

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pb-3" src="../group8.JPG" alt="">
      <span>20mm <span style="color: red"> <b>+</b> 3.17mm (Inaccuracy)</span></span>
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../group9.JPG" alt="">
      <span>40mm <span style="color: red"> <b>+</b> 5.30mm (Inaccuracy)</span></span>
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../group10.JPG" alt="">
      <span>60mm <span style="color: red"> <b>-</b> 7.90mm (Inaccuracy)</span></span>
    </div>
</div>

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pb-3" src="../group11.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../group12.JPG" alt="">
    </div>
</div>




