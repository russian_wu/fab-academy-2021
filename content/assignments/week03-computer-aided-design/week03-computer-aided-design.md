+++
title ="Week 03: Computer-Aided Design"
description = "Here is the documentation of every weeks progress."
+++

## Illustrator

I used Illustrator to draw a 春聯, spring couplet.  
<img class="img-fluid py-3" style="width: 70rem;" src="../ai1.png" alt="ai1">
<img class="img-fluid py-3" style="width: 70rem;" src="../ai2.png" alt="ai2">  
The file can be saved into different vector files.  
<img class="img-fluid py-3" style="width: 50rem;" src="../ai3.png" alt="ai3">
My spring couplet can be found [here](../spring.ai).

## GIMP

It's my first time to use GIMP, but the fuctions of it seem quite enough. While I am worry about the compatible problem of the file format, **.xcf**, it shows that the file could also export into **.png** file.  
<img class="img-fluid py-3" style="width: 70rem;" src="../gimp1.png" alt="gimp1">
<img class="img-fluid py-3" style="width: 70rem;" src="../gimp2.png" alt="gimp2">

## Photoshop

I usually create mine thumbnails with Photoshop, it can easily and quickly change the size of pictures.  
<img class="img-fluid py-3" style="width: 70rem;" src="../photoshop.png" alt="photoshop">

## OpenJSCAD

It is really amazing website, which allows me to explore the background of a 3D software. If I have more free time, I would like to dive into it deeper. The javascprit stirs up my memory of **Processing**.  
<img class="img-fluid py-3" style="width: 70rem;" src="../jscad1.png" alt="jscad1">  
The exported file is [here](../jscad.dxf).
<img class="img-fluid py-3" style="width: 70rem;" src="../jscad2.png" alt="jscad2">

## Blender 
Blender is a really popular 3D modelling software recently, not only because it is free but also the rendering quality is really well. However, after few minutes try, I just couldn't even create a sphere... I think I need to spend more time on it.  
<img class="img-fluid py-3" style="width: 70rem;" src="../blender.png" alt="blender"> 

## Fusion 360

I followed the tutorials from [Product Designer Online](https://www.youtube.com/watch?v=qvrHuaHhqHI). And the following is my documentations steps by steps.

1. Create a new project, save it into new file. It's good to create a folder to save the similar files  
	<img class="img-fluid py-3" style="width: 50rem;" src="../save1.png" alt="save1">
	<img class="img-fluid py-3" style="width: 70rem;" src="../save2.png" alt="save2">  
	Once create sucessfully, the file name would show on the file tab, like this:
	<img class="img-fluid py-3" style="width: 70rem;" src="../save3.png" alt="save3">

2. Open a new component and name it. Tips: Always create a new component when create a new part of projects  
	<img class="img-fluid py-3" style="width: 70rem;" src="../new-component.png" alt="new-component">
	<img class="img-fluid py-3" style="width: 30rem;" src="../name-component.png" alt="name-component">
	<img class="img-fluid py-3" style="width: 30rem;" src="../browser.png" alt="browser">  
	You will see the component is activated in the browser.
	   
	
3. Create a **Line** with shotcut **L**
	<img class="img-fluid py-3" style="width: 70rem;" src="../line1.png" alt="line1">  
	<img class="img-fluid py-3" style="width: 20rem;" src="../line2.png" alt="line2"> 
	The viewing point can be rotated with the cube on the right-top side.   
	<img class="img-fluid py-3" style="width: 20rem;" src="../line3.png" alt="line3"> Or on the bottom.  

	Click on the origin point, type the length of line, and click on the other place, the drawing line procedure will go on.
	<img class="img-fluid py-3" style="width: 70rem;" src="../line4.png" alt="line4">   
	Type in the length and press **Tab** to switch on  degree, finish the drawing with **ESC**.
	<img class="img-fluid py-3" style="width: 70rem;" src="../line5.png" alt="line5">  
	Right click the mouse and select **Repeat Line**.
	<img class="img-fluid py-3" style="width: 70rem;" src="../line6.png" alt="line6">  
	Finish the line.  
	<img class="img-fluid py-3" style="width: 70rem;" src="../line7.png" alt="line7">

4. **Extrude** the surface  
	<img class="img-fluid py-3" style="width: 70rem;" src="../extrude1.png" alt="extrude1">  
	Type in the length and click ok.
	<img class="img-fluid py-3" style="width: 70rem;" src="../extrude2.png" alt="extrude2">
	<img class="img-fluid py-3" style="width: 70rem;" src="../extrude3.png" alt="extrude3">

5. Insert **SVG** file  
	<img class="img-fluid py-3" style="width: 40rem;" src="../insert1.png" alt="insert1">  
	<img class="img-fluid py-3" style="width: 70rem;" src="../insert2.png" alt="insert2">  
	Select the top surface  
	<img class="img-fluid py-3" style="width: 70rem;" src="../insert3.png" alt="insert3">  
	Rotate and move the SVG file into the correct direction  
	<img class="img-fluid py-3" style="width: 70rem;" src="../insert4.png" alt="insert4">
	<img class="img-fluid py-3" style="width: 70rem;" src="../insert5.png" alt="insert5">  
	Most important thing, remeber to click the **Horizontal Filp**  
	<img class="img-fluid py-3" style="width: 70rem;" src="../insert6.png" alt="insert6">
6. Extrude the logo from SVG file  
	Select all the elements need to be extrude. Tips: **double click** whatever mis-select to unselect
	<img class="img-fluid py-3" style="width: 70rem;" src="../logo1.png" alt="logo1">  
	Type in the distance  
	<img class="img-fluid py-3" style="width: 30rem;" src="../logo2.png" alt="logo2">  
	Click **OK** button to finish the extrusion  
	<img class="img-fluid py-3" style="width: 70rem;" src="../logo3.png" alt="logo3">
7. Create a new component and name it in **Handle**  
	<img class="img-fluid py-3" style="width: 30rem;" src="../handle1.png" alt="handle1">  
	The handle component is under the nest of plate 1, we can move it out by dragging the handle on to the upper layer.  
	<img class="img-fluid py-3" style="width: 30rem;" src="../handle2.png" alt="handle2">
	<img class="img-fluid py-3" style="width: 30rem;" src="../handle3.png" alt="handle3">  

8. Construct a handle with middleplane  
	<img class="img-fluid py-3" style="width: 70rem;" src="../construction1.png" alt="construction1">  
	Select **two sides** of planes  
	<img class="img-fluid py-3" style="width: 70rem;" src="../construction2.png" alt="construction2">

9. Press **P** , **Projection**, to convert the middleplane into 2D sketch  
	<img class="img-fluid py-3" style="width: 70rem;" src="../projection1.png" alt="projection1">  
	Select the plane you want to project by clicking the **surface or outline**  
	<img class="img-fluid py-3" style="width: 70rem;" src="../projection2.png" alt="projection2">

10. Use **Offest** to create the handle from the projected plane
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset1.png" alt="offset1">  
	Type 0.25 mm depends on the 3D printer quality.  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset6.png" alt="offset6">

11. Cut the offset with **Line**
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset2.png" alt="offset2">  
	Remeber to draw the line **out** of plane and drag it back to snippet to confirm it **cut** the plane.  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset3.png" alt="offset3">  

12. Extrude the plane  
	Select the surface want to extrude  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset4.png" alt="offset4">  
	Select **Symmetric** to extrude the object to both sides  
	<img class="img-fluid py-3" style="width: 40rem;" src="../offset5.png" alt="offset5">  
	And we get this!!  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset7.png" alt="offset7"> 

13. Draw a **construction line** on the button to help with the middle circle drawing  
	<img class="img-fluid py-3" style="width: 70rem;" src="../construct-help1.png" alt="construct-help1">   
	Press **C** to activate **Circle**, and put the mouse no middel it would show a **triangle** which mean it is in the center.  
	<img class="img-fluid py-3" style="width: 70rem;" src="../construct-help2.png" alt="construct-help2"> 
	<img class="img-fluid py-3" style="width: 70rem;" src="../construct-help3.png" alt="construct-help3"> 
14. Creat another circle with **Offset Plane**  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset-plane1.png" alt="offset-plane1">   
	Select the plane you want to offset and type in 60 mm.  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset-plane2.png" alt="offset-plane2">   
	Press **C** and type in 30 mm to draw the circle  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset-plane3.png" alt="offset-plane3">   
	We will get this  
	<img class="img-fluid py-3" style="width: 70rem;" src="../offset-plane4.png" alt="offset-plane4"> 
15. Use **Loft** comand to connect the dots  
	<img class="img-fluid py-3" style="width: 70rem;" src="../loft1.png" alt="loft1">   
	Select two circles  
	<img class="img-fluid py-3" style="width: 70rem;" src="../loft2.png" alt="loft2">   
16. Use **Fillet** to create rounded edges to beutify the handle.  
	<img class="img-fluid py-3" style="width: 70rem;" src="../fillet1.png" alt="fillet1">   
	Select the **edge** and type in 10 mm, press **+** to select another edge to **fillet in different size**  
	<img class="img-fluid py-3" style="width: 30rem;" src="../fillet2.png" alt="fillet2">   
	<img class="img-fluid py-3" style="width: 70rem;" src="../fillet3.png" alt="fillet3">   
	Then it's done!!   
	<img class="img-fluid py-3" style="width: 70rem;" src="../fillet4.png" alt="fillet4"> 
17. Export into **STL** file, by right clicking on the component and select **Save as STL**  
	<img class="img-fluid py-3" style="width: 70rem;" src="../stl1.png" alt="stl1">  
	<img class="img-fluid py-3" style="width: 30rem;" src="../stl2.png" alt="stl2">
	<img class="img-fluid py-3" style="width: 30rem;" src="../stl3.png" alt="stl3">  

- Then we get the [Plate](../plate.stl) and [Handle](../handle.stl) files here.
- SVG file for the stamp is [here](../russian-stamp.svg)
- The final .fbx of the stamp is [here](../stamp_russian.fbx)










