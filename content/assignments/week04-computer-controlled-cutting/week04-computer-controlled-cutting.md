+++
title ="Week 04: Computer-Controlled Cutting"
+++
<img class="img-fluid pb-3" src="../present.jpg" alt="present">

## Assignment
- Group Assignment: 
	- characterize your lasercutter's focus, power, speed, rate, kerf, joint clearance and types 
- Individual assignment:
	- cut something on the vinylcutter
	- design, lasercut, and document a parametric construction kit,
		accounting for the lasercutter kerf, which can be assembled in multiple ways, and for extra credit include elements that aren't flat

The documentation of **Group Assignments** could be found [here](https://mensahdann.gitlab.io/fab-academy/assignments/a4g/).

## Vinyl Cutter

<img class="img-fluid py-3" style="width: 20rem;" src="../vinylcutter5.JPG" alt="vinylcutter5">
<img class="img-fluid py-3" style="width: 40rem;" src="../vinylcutter1.JPG" alt="vinylcutter1">

Vinyl cutter as its name impplies, it cuts espescially for the vinyl stickers. One thing needs to notice is that the machine only accepts **vector files**. The vinyl materials usually are in one color, if I want to produce the stickers in mutiple colors, I need to cut it seperately.  
For example, the face color of smile sticker need to be in yellow but the eyes and mouth in black. So I opened three layers in Illustrator to seperate each elements.
<img class="img-fluid py-3" style="width: 70rem;" src="../vinylcutter7.png" alt="vinylcutter7">  
1. Cut the facial feature and frame, and use the **Transfer Tape** to locate them on the face.  
<img class="img-fluid py-3" style="width: 40rem;" src="../vinylcutter2.JPG" alt="vinylcutter2"> 
<img class="img-fluid py-3" style="width: 30rem;" src="../vinylcutter3.JPG" alt="vinylcutter3"> 
2. Then we got this!  
<img class="img-fluid py-3" style="width: 30rem;" src="../vinylcutter4.JPG" alt="vinylcutter4"> 
3. Tear it off gentlely, then attach to anywhere you want!  
<img class="img-fluid py-3" style="width: 30rem;" src="../vinylcutter6.JPG" alt="vinylcutter6"> 

Smile sticker is available [here](../smile.ai).

## Heat Press

Basicly Heat Press shares the same producing procedure with Vinyl Cutter, but the material is replaced into **heat transfer vinyl**.  
<img class="img-fluid py-3" style="width: 30rem;" src="../heatpress2.JPG" alt="heatpress2"> 
<img class="img-fluid py-3" style="width: 30rem;" src="../heatpress1.JPG" alt="heatpress1">  
After the heat increases to 150 celcius degree, we can put the clothe under it. First to press without the sticker to iron the clothe and then put on the vinyl sticker on the clothe and press for 10s. Then it's done!  
<img class="img-fluid py-3" style="width: 30rem;" src="../heatpress3.jpg" alt="heatpress3"> 

## Laser Cutter

The introduction of Lasser Cutter can be seen from the [group assignments](https://mensahdann.gitlab.io/fab-academy/assignments/a4g/), which is done by Dann Mensah, Daniel Wilenius and me. So here I will go directly into the documentation of my individual assignment.  
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter1.JPG" alt="lasercutter1">  

Last Thursday (16.02) was Chinese New Year, in the special days we always decorate our home with **春聯, Spring Couplets**. Usually with hand-writed calligraphy on the red posters, we write down some blessing words or couplets.   
For example in the following pictures, on the sides of door **桃李杏春到萬家，松竹梅歲迎千友** which means, "Apricots and plums that the fruits represent spring season is coming to us, pines, bamboos and plum blossoms that represent summer welcome my friends."  
<img class="img-fluid py-3" style="width: 30rem;" src="../chunlian1.png" alt="chunlian1">
<img class="img-fluid py-3" style="width: 30rem;" src="../chunlian2.jpg" alt="chunlian2">   
Also, writing in one word, 福(Good fortune) and 春(/chun/,Spring) is another options. As you can see in the right picture, we sometime **reverse** the words, we say **倒(dǎo)**, that is the homophonic from **到(dào)** with means **arrive.**  So when you see a **reverse spring (春)**, which means we welcome the coming spring!  
<img class="img-fluid py-3" style="width: 30rem;" src="../chunlian3.jpg" alt="chunlian3">
<img class="img-fluid py-3" style="width: 30rem;" src="../chunlian4.jpg" alt="chunlian4">  
In my assignment, I started the design from Illustrator.  
<img class="img-fluid py-3" style="width: 70rem;" src="../lasercutter2.png" alt="lasercutter2">
By imerging the charactor with the outline, the words can be more steady. To make a standable 3D spring couplet, I made a **press-fit** joint that I learned from the group assignment. And I used **1.9mm** gap for the **2mm** plywood, which is the result from my group experiment.  
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter3.JPG" alt="lasercutter3">
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter4.JPG" alt="lasercutter4">  
Then we started cut it in laser cutter.  
<video class="img-fluid py-3" controls width="300">
	<source src="../lasercutter5.mp4" type="video/mp4">
</video>     
Then we got the result in couple minutes.  
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter6.JPG" alt="lasercutter6">
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter7.JPG" alt="lasercutter7">  

Assemble them, they look really nice!  
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter8.JPG" alt="lasercutter8"> 
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter9.JPG" alt="lasercutter9"> 
<img class="img-fluid py-3" style="width: 30rem;" src="../lasercutter10.JPG" alt="lasercutter10">  

## ? Weakness
After the first experiment, I awared there are some points could be improved. 
1. Because of the charactor, joint stucks a lot. 
2. Whenever I tried to change the thickness of the plywood, I need to change the whole design again.
3. The sideview of my standiny spring couplet isn't symmetry, though it can present the 春 and reverse one in the simultaneously.

Solution:
1. Create a track for joints
2. Use the paremetric design from Fusion 360
3. Build in same direction and standing points for both sides.

## ! Let's improve it
Insert the 春 in svg file into Fusion. And modify the word to connect with the outline.
<img class="img-fluid py-3" style="width: 70rem;" src="../improve1.png" alt="improve1">  
Cut a gap and build a track with set the **parameters**, which allows us easier to change in the later modification.  
<img class="img-fluid py-3" style="width: 70rem;" src="../improve2.png" alt="improve2">   
Cut suppoters on both sides, and extrude with the parameter, thickness.  
<img class="img-fluid py-3" style="width: 70rem;" src="../improve3.png" alt="improve3">  
Extrude the sketch, and do the same procedure on another side.  
<img class="img-fluid py-3" style="width: 30rem;" src="../improve4.png" alt="improve4">
<img class="img-fluid py-3" style="width: 30rem;" src="../improve5.png" alt="improve5">  
Export the svg file with a plug-in, **[Shaper Utilities](https://apps.autodesk.com/FUSION/en/Detail/Index?id=3662665235866169729&appLang=en&os=Mac)**.  
<img class="img-fluid py-3" style="width: 70rem;" src="../improve6.png" alt="improve6">  
Then the problems solved!  
<img class="img-fluid py-3" style="width: 50rem;" src="../improve7.JPG" alt="improve7">

## ! Show time

<img class="img-fluid py-3" style="width: 30rem;" src="../show3.JPG" alt="show3">
<img class="img-fluid py-3" style="width: 30rem;" src="../show4.JPG" alt="show4">  
<img class="img-fluid py-3" style="width: 70rem;" src="../show1.JPG" alt="show1">
<img class="img-fluid py-3" style="width: 70rem;" src="../show2.JPG" alt="show2">  

Color it up with the red, which means blessing and brings us the luckness!  
<img class="img-fluid py-3" style="width: 30rem;" src="../show5.JPG" alt="show5">  
<img class="img-fluid py-3" style="width: 30rem;" src="../show6.jpg" alt="show6 ">  
<video class="img-fluid py-3" controls width="500">
	<source src="../show8.mp4" type="video/mp4">
</video>
<video class="img-fluid py-3" controls width="500">
	<source src="../show9.mp4" type="video/mp4">
</video>  
Try in acrylic!  
<img class="img-fluid py-3" style="width: 30rem;" src="../show7.JPG" alt="show7">

## ? The problems

- While I imported the svg file into Fusion 360, the size was changed.
The answer is [here](https://knowledge.autodesk.com/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/SVG-file-imports-with-a-wrong-scale-into-Fusion-360.html).
- The sketch of gap I created with **Square Root fuction**.
The function could be found [here](https://knowledge.autodesk.com/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/Using-Square-Root-function-in-Parameter-Expressions-in-Fusion-360.html).
- The **2mm acrylic** is fragile and couldn't be cutted well by laser cutter, although I set the speed:35, power:100, frequency:5000. And it is always smelly.  
<img class="img-fluid py-3" style="width: 30rem;" src="../problem1.JPG" alt="problem1">


The spring couplets files are provided [here](../spring-d.svg) and [here](../spring-t.svg).




