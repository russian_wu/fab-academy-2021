+++
title ="Week 13: Molding and Casting"
+++

<img class="img-fluid pb-3" src="../present.jpg" alt="">


## Assignment

- Group assignment:
  - review the safety data sheets (SDS) for each of your molding and casting materials
  - compare test casts with each of them

- Individual assignment:
  - Design a mold around the stock and tooling that you'll be using
  - Mill the mold (rough cut + finish cut)
  - Cast it

## Group Assignment

#### Safety Data Sheets (SDS)

<div class="row py-3">
    <div class="col">
      <img class="img-fluid" src="../sds1.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../sds2.JPG" alt="">
    </div>
</div>

#### Compare test cast

<img class="img-fluid py-3" style="width: 30rem;" src="../compare2.JPG" alt="">

The left cube was cracked becasue of **wrong ratio of A and B volume**. The right one is more hard and strong. So the ratio, depends on every material, by **volumen or weight** is critical and it directly affects the casting results.

<video class="img-fluid" controls width="300">
  <source src="../compare1.mp4" type="video/mp4">
</video>

The broken cube was soft but not entirely flexible.

***

## Individual assignment

#### Design a mold

According to the need of my final project, I decided to build inflatable silicone. I found a documentation from [Avner Peled](https://wiki.avner.us/doku.php?id=soft-robotics:molds:fast-bending), he did some research on Soft Robot. From the [soft robotic toolkits website](https://softroboticstoolkit.com/book/pneunets-downloads), I downloaded the 3D model and did a slightly modification in Fusion 360 to fit it into my material. The **assemble instruction** could be found [here](https://softroboticstoolkit.com/book/pneunets-fabrication).

<div class="row py-3">
    <div class="col">
      <img class="img-fluid" src="../design1.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../design2.png" alt="">
    </div>
</div>


#### Mill the mold (roughing and finishing)

In order to mill a mold, there are several ways. FreeCAD, Fusion 360 and [VCarve](https://www.youtube.com/watch?v=X94lQ8_Um1Y) all provide the function to create the toolpaths for CNC machine. I chose Fusion to establish my toolpaths. The principles are similar to the previous works: [Week 07: Electronics Design](../../week07-electronics-design/week07-electronics-design) and [Week 08: Computer-Controlled Machining](../../week08-computer-controller-machining/week08-computer-controller-machining). After creating a model in Fusion, we need to **setup** the size of our material. But this time we need the **Pocket clearing** to rough and **Parallel** to finish my mold.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../mill1.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../mill2.png" alt="">
    </div>
</div>

###### Important setting for Setup:

<img class="img-fluid pb-3" style="width: 15rem;" src="../mill3.png" alt="">

###### Important setting for Pocket clearing (Rough):

Roughing is to speed up the whole milling proccedure by quick and rough milling, thus we use higher **stepover** to realize it in shorter time. For example, I used **3mm bit** and the most suitable stepover could be 1.0-1.5mm. In the case of roughing, **Flat End bit** is quite sufficient. I used **3mm 2 flute flat end** bit for my mold.

<img class="img-fluid pb-3" style="width: 30rem;" src="../mill26.JPG" alt="">

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pb-3" src="../mill6.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../mill7.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../mill8.png" alt="">
    </div>
</div>

The speed and feedrate setting was always a problem for me, it more relies on personal experiences, machines and materials. I found a tutorial explains more detail about of [speed and feed](https://www.youtube.com/watch?v=gTnkNHB7dss). If I have time I might need to see more tutorials about it.

The material, wax block and sikablock, is softer than MDF and plywood. So we could set the **spindle speed** close to maximun according to the machine. Set the **feedrate** according to the materials, **vertical feedrate** could be half of feedrate.
In my case, I used both MDX-40 and SRM-20 for different part of my mold.

###### Roughing in [MDX-40](https://www.pdi3d.com/v/vspfiles/files/MDX-40A%20Brochure.pdf) with Wax block:

<img class="img-fluid pb-3" style="width: 35rem;" src="../mill9.png" alt=""><br>
<img class="img-fluid pb-3" style="width: 40rem;" src="../mill4.png" alt=""><br>
<img class="img-fluid pb-3" style="width: 40rem;" src="../mill5.png" alt="">
<img class="img-fluid pb-3" style="width: 40rem;" src="../mill18.png" alt="">


###### Roughing in [SRM-20](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine#specs) with [Sikablock M330](https://www.jacomp.fi/wp-content/uploads/2016/09/SikaBlock_overview_and_milling_parameters_2014.pdf):

<img class="img-fluid pb-3" style="width: 35rem;" src="../mill10.png" alt="">
<img class="img-fluid pb-3" style="width: 30rem;" src="../mill11.png" alt="">
<br>
<img class="img-fluid pb-3" style="width: 40rem;" src="../mill12.png" alt="">
<br>
<img class="img-fluid pb-3" style="width: 40rem;" src="../mill13.png" alt="">

<img class="img-fluid pb-3" style="width: 40rem;" src="../mill19.png" alt="">


###### Important setting for Parallel (Finish):

Finishing is to sand the surface of mold by gradual and detail milling, thus lower stepover is neccesary to have a fine surface. Usually to finish some detail mold, we would need a **ball end bit** to smooth the surface cleanly. But in my case, the mold was mainly combinded with flat surface; therefore, 3mm flat end bit is quite enough, further in the contious milling from roughing to finishing I don't need to change the bit in the middle of milling.


###### Finishing in [MDX-40](https://www.pdi3d.com/v/vspfiles/files/MDX-40A%20Brochure.pdf) with Wax block:

<img class="img-fluid pb-3" style="width: 15rem;" src="../mill14.png" alt="">

<img class="img-fluid pb-3" style="width: 15rem;" src="../mill15.png" alt="">

<img class="img-fluid pb-3" style="width: 40rem;" src="../mill20.png" alt="">

###### Finishing in [SRM-20](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine#specs) with [Sikablock M330](https://www.jacomp.fi/wp-content/uploads/2016/09/SikaBlock_overview_and_milling_parameters_2014.pdf):

<img class="img-fluid pb-3" style="width: 15rem;" src="../mill16.png" alt="">

<img class="img-fluid pb-3" style="width: 15rem;" src="../mill17.png" alt="">
<img class="img-fluid pb-3" style="width: 40rem;" src="../mill21.png" alt="">

#### Setting in MDX-40 and SRM-20

###### MDX-40

<img class="img-fluid pb-3" style="width: 30rem;" src="../mill23.JPG" alt="">
<img class="img-fluid pb-3" style="width: 30rem;" src="../mill22.JPG" alt="">

###### SRM-20

<img class="img-fluid pb-3" style="width: 30rem;" src="../mill24.JPG" alt="">
<img class="img-fluid pb-3" style="width: 30rem;" src="../mill25.JPG" alt="">

#### !Start (Left MDX-40+Waxblock/Right SRM-20 Sikablock)

<div class="row pb-3">
    <div class="col-7">
      <img class="img-fluid" src="../start1.JPG" alt="">
    </div>
    <div class="col-5">
      <img class="img-fluid" src="../start2.JPG" alt="">
    </div>
</div>

###### Roughing 

<div class="row pb-3">
    <div class="col-7">
      <video class="img-fluid">
        <source src="../start1.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col-5">
      <video class="img-fluid">
        <source src="../start2.mp4" type="video/mp4">
      </video>
    </div>
</div>

##### Finishing

<div class="row pb-3">
    <div class="col-7">
      <video class="img-fluid">
        <source src="../start3.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col-5">
      <video class="img-fluid">
        <source src="../start8.mp4" type="video/mp4">
      </video>
    </div>
</div>

##### Molds done

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../start9.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../start6.JPG" alt="">
      <img class="img-fluid" src="../start10.JPG" alt="">
    </div>
</div>


## Casting in the mold

<img class="img-fluid py-3" src="../cast8.JPG" alt="">

Before start the casting, it is madotory to read <span style="color: red"><b>Safety Data Sheets (SDS)</b></span> to know how to work with **Mold Star**. Wearing gloves is always neccessary during the all mixing procedure.

<img class="img-fluid pb-3" style="width: 30rem;" src="../cast9.JPG" alt="">

Measure the weight of water that filled up my mold to know how much silicone I need. According to the instruction on SDS to mix **A and B bottle 1:1 by weight**. **Shake it properly before you mix it!!**

<video class="img-fluid" controls width="500">
  <source src="../cast11.mp4" type="video/mp4">
</video>

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../cast1.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../cast2.JPG" alt="">
    </div>
</div>

Use the vacuum to squeeze out the bubble from mixture.

<div class="row pb-3">
    <div class="col-7">
      <img class="img-fluid" src="../cast3.JPG" alt="">
    </div>
    <div class="col-5">
      <img class="img-fluid" src="../cast4.JPG" alt="">
    </div>
</div>

<span style="color: red"><b>!!Waring:</b> The valve supposed to be open gradually to avoid the spitting of mixture.</span>

<div class="row pb-3">
    <div class="col">
      <video class="img-fluid">
        <source src="../cast7.mp4" type="video/mp4">
      </video>
      Wrong!
    </div>
    <div class="col">
      <video class="img-fluid">
        <source src="../cast10.mp4" type="video/mp4">
      </video>
      Correct!
    </div>
</div>


Casting slowly with thiner fall, the bubbles is easier to come out and the silicone is easier to fill up every gap. Depends on different materials, for **Mold start 16 Fast** after 30 minitues could finish the cure.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../cast5.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../cast6.jpg" alt="">
    </div>
</div>


## Challenges


1. The first molds weren't cut off totally and also the wax ash was too fine to remove. While I were cleaning the bottom of mold, it was broken into pieces by the chisel.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../challenge2.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../challenge3.JPG" alt="">
    </div>
</div>

**!! Sollution:**
Glue it up with epoxy glue. Next time, Increase the **stepover into 1mm for roughing** and set **Axial Stock to Leave into -0.5mm**.

<img class="img-fluid pb-3" style="width: 50rem;" src="../challenge4.JPG" alt=""> 

2. The second molds were slightly broken while finishing. 

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../challenge5.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../challenge6.JPG" alt="">
    </div>
</div>

**?? Reason:**

The z-axis of MDX-40 was changed during the roughing and finishing procedure. Might becasue of the long processing time.

**!! Sollution:**

Fix with soldering gun to melt the wax ash and fill the broken part up.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../challenge7.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../challenge8.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../challenge9.JPG" alt="">
    </div>
</div>

3. The silicone was dried in 6 minutes.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../challenge10.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../challenge11.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../challenge15.JPG" alt="">
    </div>
</div>

**!! Sollution:**
After mixing the silicone, first cast the silicone into molds. Then put the mold and casting into vacuum together.

4. The molds broke again because of the dried silicone was attached with molds without any space.

<div class="row pb-3">
    <div class="col-3">
      <img class="img-fluid" src="../challenge12.JPG" alt="">
    </div>
    <div class="col-3">
      <img class="img-fluid" src="../challenge13.JPG" alt="">
    </div>
    <div class="col-6">
      <img class="img-fluid" src="../challenge14.JPG" alt="">
    </div>
</div>

**!! Sollution:**
Use **talc or Baby powder** before pouring the silicone. The casting result would be bettero.




## Download

<img class="img-fluid pb-3" style="width: 40rem;" src="../design1.png" alt="">

- [CavityMold](../CavityMold.f3d) from [soft robotic toolkits website](https://softroboticstoolkit.com/book/pneunets-downloads)

- [BaseMold](../BaseMold.f3d) from [soft robotic toolkits website](https://softroboticstoolkit.com/book/pneunets-downloads)




