+++
title ="Week 02: Principles and Practices"
description = "Here is the documentation of every weeks progress."
+++

## Bootstrap

<div class="container py-3">
  <div class="row">
    <div class="col-md-4"><img class="img-fluid" src="../bootstrap.png" alt="bootstrap"></div>
    <div class="col-md-8"> <a href="https://getbootstrap.com">Bootstrap</a> is a open-source that allows beginners to build responsive websites faster and more organized. And the further history about Bootstrap can be found <a href="https://getbootstrap.com/docs/5.0/about/overview/">here</a>.</div>
  </div>
</div>

## Where to download it?

1. Include the link in the scripts, however it only works when you have internet.

		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
2. Download the compiled CSS and JS from [website](https://getbootstrap.com/docs/5.0/getting-started/download/).

## How to start it?

1. Copy the **bootstrap.min.css** and **bootstrap.min.js** files into the static file which we built in Hugo, becasue the scripts should be able to access by all the folders.  
<img class="img-fluid py-3" style="width: 50rem;" src="../folder.png" alt="bootstrap">

2. Add `<link rel="stylesheet" href="{{ .Site.BaseURL }}assets/css/bootstrap.min.css">` into the head of website. Min means the minified version of bootstrap library.

3. Add `<script src="{{ .Site.BaseURL }}assets/js/bootstrap.min.js"></script>` at the end of page.

Futher information could be found from [Bootstrap Documentation](https://getbootstrap.com/docs/5.0/getting-started/introduction/)


## Build a Navbar

<img class="img-fluid py-3" style="width: 70rem;" src="../navbar.png" alt="navbar">

- `<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #284fb5;"></nav>`
	- `md` is Breakpoints which are the triggers in Bootstrap for how your layout responsive changes across device or viewport sizes.
	- `navbar-dark` decides the color of words
	- `bg-light` decides the background color, which can also change into `style="background-color: #284fb5;"`

- `class="container-fluid"` enable container to be fit all the page, `class="container"` makes the container to be responsive
 The container of navbar allows bootstrap to assign class through all the content
 - Botton

 	`<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarMain"></button>`
- Dropdown

	`<li class="nav-item dropdown"></li>`

- Dropdown menu

	`<ul class="dropdown-menu"></ul>`

## Header
<img class="img-fluid py-3" style="width: 40rem;" src="../header.png" alt="header">

- `p` padding around
- `py-5` padding in y direction
- `px-5` padding in x direction
- `<h1 class="display-1"></h1>` font size in the maximum
- `<p class="lead"></p>` paragraph in the lead size

## Main
<img class="img-fluid py-3" style="width: 70rem;" src="../main.png" alt="mains">

- Read the detail of `<div class="row row-cols-2 row-cols-md-4"></div>` in [Row columns](https://getbootstrap.com/docs/5.0/layout/grid/).
- Read the detail of `<div class="card"></div>` in [Card](https://getbootstrap.com/docs/5.0/components/card/).
- `<a class="btn btn-dark" href="{{ .Site.BaseURL }}assignments/week01-project-management">View</a>` is for View button

## Footer
<img class="img-fluid py-3" style="width: 40rem;" src="../footer.png" alt="footer">

- `{{ now.Format "2006" }}` can access the time for now


## Markdown Tips

- `{{ .Page.Title }}` the title would get from ***content***

- `{{ .Site.Title }}` the title would get from ***config.toml***












