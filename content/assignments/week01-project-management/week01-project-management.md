+++
title ="Week 01: Project Management"

+++


# Day 01: HTML Basics

Today we learned begin from the breif history of World Wide Web, which was invented by [Tim Berners-Lee](https://www.w3.org/People/Berners-Lee/) on 1989.  
And Kris also explained about the relationship from **Server, DNS Server to Client**.   
Because of my habits, I selected [Sublime](https://www.sublimetext.com) as text editor, Chrome and Safari as my browesers.

Everything start from html basic structure:
```
<!DOCTYPE html>
<html>
<head>
	<title>Fab Academy 2021</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
</head>
<body>
	Hello.
</body>
</html>
```

### Something need to mention: 

	<meta charset="utf-8">
It makes browser can display for special characters.  

	<meta name="viewport" content="width=device-width">
With this script, the content will automaticly adjusts to different devices' browsers.

When we need **headings**:
```
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>
```
When we need **lists**:
```
<ul>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
</ul>
```
When we need **paragraphs**:
```			
<p>This is a paragraph.</p>
<p>This is another paragraph.</p>
```	
HTML links could be added after most of tags:

	<a href="url">link text</a>

After all the steps, we finally got this :) ↓
	
<img class="img-fluid" style="width: 50rem;" src="../HTML-basic.png" alt="HTML basic">	
	
	

### The futher information can be found :
- [MDN web docs](https://developer.mozilla.org/en-US/)
- [W3schools](https://www.w3schools.com/)
- Day 01 video could be found from [last year](https://youtu.be/Y5cm-__K3sM).  

***

# Day 02: Command Line and Git Basics

### Command Line
<img class="img-fluid" style="width: 50rem;" src="../Command_Line.png" alt="Command Line">

Second day we talked about the history of Command Line. It came from Teleprinter, teletypewriter and later became computor terminals. In the MacOSX Zsh is defaulted, so for the Mac user can easily access from **Terminal**.

<div class="container">
  <div class="row">
    <div class="col-md-4"><img class="img-fluid py-5" src="../git.png" alt="Git"></div>
    <div class="col-md-8 py-4">
    	<h3>Git</h3>
    	Git is version controller. It was created by Linus Torvalds for providing Linux kernel development an alternative. After installing it in the folder, we can use command line to update and manage old and new files to record down every steps we did.
	</div>
  </div>
</div>

**Hint:** GNU nano is a text editor for operating environments using a command line interface.
<img class="img-fluid" style="width: 50rem;" src="../Nano_interface.png" alt="Nano">

	$ nano hello.txt

### Command in Command Line
- Change the directory:
	```
	$ cd Desktop/
	```
- Stay in the same directory:
	```
	$ cd .
	```
- Go to one layer above

	```
	$ cd ..
	```
- Print out the file in current direcrory:
	```		
	$ ls
	README.md   content     layouts
	```
- List all contents in current directory including **hidden** files and directories
	```
	$ ls -a
	.              .git           README.md      layouts
	..             content        .DS_Store     
	```
- List all files with the authorities and creator information
	```
	$ ls -l
	-rw-r--r--  1 russianwu  staff  195  1 22 00:24 README.md
	drwxr-xr-x  7 russianwu  staff  224  1 28 14:01 content
	drwxr-xr-x  3 russianwu  staff   96  1 28 01:27 static
	```
- Print the path of the working directory
	```
	$ pwd
	/home/ccuser/workspace/blog
	```
- Clean the terminal screen
	```
	$ clear
	``` 
- Creat a file
	```
	$ touch data.txt
	```

### Git Installation
For macOS users to install git, please follow the instruction in the [Homebrew](https://brew.sh).  
1. Confirm the installation:
	
		$ git version
		git version 2.25.1
2. Git Initailization:

		$ git init
		$ ls -a
		. .. electronics.png .git index.html
3. **Setting the username:**
	
		$ git config --global user.name "John Doe"
		$ git config --global user.email johndoe@example.com
4. Checking the status all the time:

		$ git status
		Untracked file...
5. Adding the file to staging erea:

		$ git add filename.jpg
		$ git add . (all the file)
6. Commiting the file:

		$ git commit -m 'Initial commit'
		2 files changed...
7. Checking the history of commit:

		$ git log
7. Get out of **git log**:

		$ q
8. Checking the difference from last version:

		$ git diff
### HTML
1. Table

		<table>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
2. Image

		<img src="electronics.png" alt="Temporary Image">
3. Copyright

		&copy;
4. Links

		<a href="index.html">Home</a>
### The futher command can be found :
- [Codeacademy](https://www.codecademy.com/articles/command-line-commands#:~:text=The%20command%20line%20is%20a,or%20Finder%20on%20Mac%20OS.)
- Day 02 video could be found from [here](https://youtu.be/GFyjlPa73xU).  

***

# Day 03: Setting up and Using GitLab
	
<div class="container">
  <div class="row">
    <div class="col-md-4"><img class="img-fluid" src="../Gitlab.jpg" alt="GitLab"></div>
    <div class="col-md-8 py-4">
    	<h2>GitLab</h2>
    	<p>GitLab is a web-based DevOps tool and developed by Ukrainian developers: Valery Sizov and Dmitriy Zaporozhets. Git repository hosting implements into the DevOps cycle : <b>Build → Test → Result</b></p></div>
  </div>
</div>

### SSH public key (Secure Shell)
GitLab needs your ssh public key for decoding your information.
It can be set by following steps:
1. Generate SSH key:

		$ ssh-keygen -t ed25519 -C "My Key"
follwing the default without the passphrase.

2. Print SSH public key:

		$ cat id_ed25519.pub
3. Copy the result and past in the GitLab:
Go by Profile icon → Setting → SSH Keys → Add key

### Command Line
1. remove

		$ rm id_ed25519 id_ed25519.pub
### Creat new project in Gitlab
1. Create blank project
2. Project name: Fab Academy
3. Visibility Level: Public
4. Push an exisiting Git repository

		$ git remote add origin git@gitlab.com:russianwu/fab-academy.git
		$ git push -u origin --all
5. Everytime want to update the new file:

		$ git push
6. In order to download all the updated file:

		$ git pull
### Markdown
It is a simplify version of HTML.
Further information could be found [here](https://www.markdownguide.org/).

### .gitlab-ci.yml
Automatically publish the website:
1. click button **+**
2. .gitlab-ci.yml
3. HTML
4. Enter commit message
5. Commit change
6. Check the running pipeline  
	<img class="img-fluid py-3" style="width: 60rem;" src="../gitlab-ci.yml-result.png" alt=".gitlab-ci">

### Open the website
From setting → Pages → https://russian_wu.gitlab.io/fab-academy-2021

Day 03 video could be found from [here](https://youtu.be/sfFYqPGfUZU).

***

# Day 04: Using a Static Website Generator
**Static web page** is delivered to the user's web browser exactly as stored.  
**Dynamic web page** could show the information according to the user input data.

### [Hugo](https://gohugo.io/) 
Hugo builds with GoLang and has no dependencies.

### Hugo Installation

1. Download Hugo

	Latest version could be downloaded from [here](https://github.com/gohugoio/hugo/releases).
2. Extract hugo to wherever you want
3. Check if the hugo executable:

		$ ./hugo version
	If it pops up the following similar context, then it works.

		Hugo Static Site Generator v0.80.0-792EF0F4 darwin/amd64 BuildDate: 2020-12-31T13:36:47Z
4. Find a program location by

		$ which ls
		$ which mv
		/bin/rm
5. Move the hugo into user program location with **Super User Do (sudo)**

		$ sudo mv hugo /usr/local/bin
6. Confirm the location of hugo

		$ which hugo
		/usr/local/bin/hugo
7. Then hugo can access in the computor from anywhere

		$ hugo version
		Hugo Static Site Generator v0.80.0-792EF0F4 darwin/amd64 BuildDate: 2020-12-31T13:36:47Z

### Implment hugo into our file

1. hugo new site ./ --force
2. Get rid of the files we don't need: archetypes, data, and themes
3. Edit config.toml file
	
	<img class="img-fluid py-3" style="width: 60rem;" src="../config.png" alt="config">
4. Move index.html into layouts file
5. Check the lastest changes

		$ hugo server
6. Open http://localhost:1313/fab-academy-2021/
7. Replace the varies content with **variables** and edit **params**

	<img class="img-fluid py-3" style="width: 60rem;" src="../index-variable.png" alt="index-variable">
8. Create a partials folder for navigation.html
		
	<img class="img-fluid py-3" style="width: 60rem;" src="../navigation.png" alt="navigation">
9. Create **markdown** files in content directory

	<img class="img-fluid py-3" style="width: 30rem;" src="../content.png" alt="content">
10. Create single.html in **_default folder**

	<img class="img-fluid py-3" style="width: 60rem;" src="../single.png" alt="single">
11. Change the navitigation path

	<img class="img-fluid py-3" style="width: 60rem;" src="../navigation.png" alt="navigation">
12. The files in the static would be accessable index.html file
13. Move README.md file in to the root

		$ mv .temp/README.md ./
14. Create a .gitignore to write down the file need to be ignored

		$ touch .gitignore
15. delete the old **.gitlab-ci.yml** file
16. Create a new .gitlab-ci.yml

	1. click button **+**
	2. .gitlab-ci.yml
	3. **Hugo**
	4. Enter commit message
	5. Commit change
	6. Check the running pipeline

Day 04 video could be found from [here](https://youtu.be/7wCidM35tp8).

***

# Day 05: Optimizing Images and Videos
### Two types of images

Vector (.svg) and Raster (.png, .jpg) files are wildly used in the internet. However, there are some sepcialities for each. Png files can store the transparent picture. Jpg stores the files effective. Svg and ai files could zoom into any size without crap resolutions.

### Videos

Videos basicly are the sequence of images and playing together, and eyes working system would consider it as a continual video. The most popular compresssion file nowaday is H.264 file, it can minimize the file size and maximize the video quality.

### Choose for ducumentations

Because I am working on MacOS, I usually use **Quicktime and Preview** to screenshot or record the monitor. For video editing, I usually use **Premiere or even After Effect** when I need it. After the class I also tried with ImageMagick for image conpression. It is really amazing and effective, you can see from the following pictures.
- Install imagemagick with Homebrew:
	```
	$ brew install imagemagick
	```
- See the data of images:
	```
	$ identify DSC05173.jpg 
	```
- Resize the images:
	```
	$ convert -resize 60% DSC05173.jpg test.jpg
	$ convert -resize 1280x720 DSC05173.jpg test.jpg
	$ convert -quality 65 DSC05173.jpg test.jpg
	```
<img class="img-fluid py-3" style="width: 60rem;" src="../image-resize.png" alt="image-resize">

- Install ffmpeg with Homebrew:
	```
	$ brew install ffmpeg
	```
- See the data of videos:
	```
	$ ffmpeg -i test.mp4
	```
- Resize videos
	```
	$ ffmpeg -i IMG_2228.mov -vf scale=1280x720 video-resized.mp4
	```
- Edit videos
	```
	$ ffmpeg -i IMG_2228.mov -ss 4 -t 5 -vf scale=1280x720 video-resized-5s.mp4
	```
### Insert a shortcodes
	
		$ {{ <video> }} video-resized-5s.mp4{{ </video> }}

<img class="img-fluid" style="width: 30rem;" src="../shortcodes.png" alt="shortcodes">
<img class="img-fluid py-3" style="width: 60rem;" src="../video.png" alt="video">

### The futher information can be found :
- Information of [Video tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video).
- Day 05 video could be found from [here](https://youtu.be/Q1xU3PMvlk0).













