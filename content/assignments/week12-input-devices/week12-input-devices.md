+++
title ="Week 12: Input Devices"
+++

<div class="row pb-3">
    <div class="col">
      <video class="img-fluid" controls width="800">
    <source src="../press1.mp4" type="video/mp4">
</video>
    </div>
    <div class="col">
      <video class="img-fluid pb-3" controls width="800">
    <source src="../loading1.mp4" type="video/mp4">
</video>
    </div>
</div>

## Assignment

- Individual assignment:
    - Measure something: add a sensor to a microcontroller board that you have designed and read it

- Group assignment:
    - Probe an input device's analog levels and digital signals

## Step Response

According to the need of my [Fianl Project](../../../final-project/final-project/), I decided to use the **Step Response** as my input device for this week. On the Global Lecture Adrian shared his experiment with [Step Response](https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#step) from last year. I learned a lot from it.

To start my assignment as soon as possible, I milled my board with the trace provided by Adrian.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../step1.jpg" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../step2.jpg" alt="">
    </div>
</div>

<img class="img-fluid pb-3" style="width: 30rem;" src="../step3.JPG" alt="">

I tore off 2 of the cables to fit them into the **2.54mm pitch connector** which has blades to cut through the **Over Sheath**.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pb-3" src="../step4.JPG" alt="">
    </div>
    <div class="col">
      <video class="img-fluid pb-2" controls width="800">
        <source src="../step6.mp4" type="video/mp4">
      </video>
    </div>
</div>

With a pair of copper sheets soldering with the cables,  and then my sensor was done!

<img class="img-fluid pb-3" style="width: 60rem;" src="../step7.JPG" alt="">

Once I tried to connect the sensor to my [helloboard](../../week07-electronics-design/week07-electronics-design), I realized that I don't have any female headers on my helloboard... Adrian designed his [Adrianino](http://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html) to multiple functions and compatible with most of situations. I need my own board with **microcontroller** as well.

<img class="img-fluid pb-3" style="width: 60rem;" src="../step5.JPG" alt="">
<img class="img-fluid pb-3" style="width: 60rem;" src="../step8.jpg" alt="">

### Design my Step Response

To design my own step response sensor, I followed [Ardian's experiment](https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#step), [Robert Hart's tutortial](https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/index.html) and [Neil's examples](http://academy.cba.mit.edu/classes/input_devices/step/hello.txrx.45.mp4).

#### ?How does it work

<img class="img-fluid pb-3" src="../principle1.JPG" alt="">

A series of electric pulse is sent through the TX electrode, and it allows some of pulses pass the **electric filed**. Depends on what kind of material in between the amount of passing electronic is different. 

#### ?Voltage Divider

The (optional) resistors between 5V and GND make the **signal stabler**. The input to the ADC is attached to the point halfway betwen 5V(3.3V) and ground, and is therefore at 2.5 volts, except for the transient blips that occur at the tx electrode changes voltage. These resistors should be **equal**, and should have values of 1 mega-ohm or larger.

Further caculation could be found from [Khan Academy](https://www.khanacademy.org/science/electrical-engineering/ee-circuit-analysis-topic/ee-resistor-circuits/v/ee-voltage-divider)

#### Microcontroller

Base on different microcontroller we used, we could connect the **TX and RX** into different pins. In Step Respone sensor case, we need **Analog pin** to send and receive the pulse. Adrian chose PA5 and PA6 from ATtinyX14. It is also presented in his code.

<img class="img-fluid pb-3" src="../my2.jpg" alt="">
<img class="img-fluid pb-3" src="../my3.jpg" alt="">
<img class="img-fluid pb-3" style="width: 40rem;" src="../my4.png" alt="">

I chose **ATtiny412** as my microcontroller and changed the pins in my schematic and code according to the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny212-412-DataSheet-DS40001911B.pdf) and [SpenceKonde](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md). 

<div class="row pb-3">
    <div class="col-3">
      <img class="img-fluid" src="../my8.png" alt="">
    </div>
    <div class="col-9">
      <img class="img-fluid" src="../my5.gif" alt="">
    </div>
</div>


<div class="row pb-3">
    <div class="col-4">
      <img class="img-fluid" src="../my1.JPG" alt="">
    </div>
    <div class="col-8">
      <img class="img-fluid" src="../my6.png" alt="">
    </div>
</div>


#### Bypass Capacitor (Decoupling Capacitors)

**Decoupling Capacitors**, also referred to as a **Bypass capacitor**, are a pair of capacitors we usually put besides microcontroller to stablize the incoming voltage. It acts as **energy reservoir.** In the reality the voltage would have some noise which would lead to unstable performance of microcontroller and also shorten the life time of it in long-term.


When a decoupling capacitor is in place, it will do one of two things:
1. If the input voltage drops, then a decoupling capacitor will be able to provide enough power to an IC to keep the voltage stable.
2. If the voltage increases, then a decoupling capacitor will be able to absorb the excess energy trying to flow through to the IC, which again keeps the voltage stable.

<img class="img-fluid" style="width: 40rem;" src="../my7.png" alt="">   

(picture from Eagle Academy)

In our case, we only use **one capacitor (10uF)** to symplify the circuit but also keep the stablizing function. 

Further explaination could be founded from [Eagel Academy](https://www.autodesk.com/products/eagle/blog/what-are-decoupling-capacitors/).


#### !Analog-to-Digital Converter (ADC)

<img class="img-fluid py-2" src="../adc3.png" alt="">  

Microcontrollers **can’t** read values unless it’s digital data. This is because microcontrollers can only see “levels” of the voltage, which depends on the **resolution of the ADC** and **the system voltage**.

<img class="img-fluid" style="width: 40rem;" src="../adc1.jpg" alt="">  


The **ADC’s resolution** can be tied to the precision of the ADC.

<img class="img-fluid pb-3" style="width: 40rem;" src="../adc2.jpg" alt="">  

Further information could be found from [Engineering Resources: Basics of Analog-to-Digital Converters](https://www.arrow.com/en/research-and-events/articles/engineering-resource-basics-of-analog-to-digital-converters#:~:text=ADCs%20follow%20a%20sequence%20when,its%20sampling%20rate%20and%20resolution.)

#### ?Vcc vs Vdd

In the datesheet and a lot of schematic we can see **Vcc** and **Vdd** almost everywhere. I was so confused what the different in between. The answer from [What is the difference between Vcc, Vdd, Vee, Vss?](https://electronics.stackexchange.com/questions/17382/what-is-the-difference-between-v-cc-v-dd-v-ee-v-ss) explains that "In practise today **Vcc (collector or commom collerctor voltage) and Vdd (drain) mean the positive voltage (+)**, and **Vee (emitter) and Vss (source) mean the ground (-)."**

#### ?Direction of LED

There are two ways to recognize the direction of LEDs. One is from top view to see the <span style="color: green">green line</span>of LED, which is ground. Another one is to check the [datasheet](https://media.digikey.com/pdf/Data%20Sheets/Lite-On%20PDFs/LTST-C230TBKT.pdf) directly.

<img class="img-fluid py-3" src="../my17.png" alt="">

### Step Response Schematic

<img class="img-fluid py-3" src="../my9.png" alt="">
<img class="img-fluid pb-3" src="../my10.png" alt="">

Create the rml with [mods](http://mods.cba.mit.edu/), there is new version of [Mods Projects](http://modsproject.org/).

<img class="img-fluid" src="../my11.png" alt="">
<img class="img-fluid pb-3" src="../my12.png" alt="">

<video class="img-fluid" controls width="800">
    <source src="../my13.mp4" type="video/mp4">
</video>

5x Speed Up

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../my14.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../my15.JPG" alt="">
    </div>
</div>

### Soldering

<img class="img-fluid pb-3" style="width: 30rem;" src="../my16.JPG" alt=""> 

### Adruino Code

```
//tx_rx03  Robert Hart Mar 2019.
//https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/index.html
//Step Response TX, RX by Adrián Torres Omaña
//Fab Academy 2021
//Step Response TX, RX
//Adrianino
//https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#step

//Modified by Russian Wu
//Fab Academy 2021
//Step Response TX, RX

//  Program to use transmit-receive across space between two conductors.
//  One conductor attached to digital pin, another to analog pin.
//
//  This program has a function "tx_rx() which returns the value in a long integer.
//
//  Optionally, two resistors (1 MOhm or greater) can be placed between 5V and GND, with
//  the signal connected between them so that the steady-state voltage is 2.5 Volts.
//
//  Signal varies with electric field coupling between conductors, and can
//  be used to measure many things related to position, overlap, and intervening material
//  between the two conductors.
//


long result;   //variable for the result of the tx_rx measurement.
int analog_pin = 2; //  PA1 of the ATtiny412
int tx_pin = 3;  //     PA2 of the ATtiny412


void setup() {
pinMode(tx_pin,OUTPUT);      //Pin 2 provides the voltage step
Serial.begin(115200);
}


long tx_rx(){         //Function to execute rx_tx algorithm and return a value
                      //that depends on coupling of two electrodes.
                      //Value returned is a long integer.
  int read_high;
  int read_low;
  int diff;
  long int sum;
  int N_samples = 100;    //Number of samples to take.  Larger number slows it down, but reduces scatter.

  sum = 0;

  for (int i = 0; i < N_samples; i++){
   digitalWrite(tx_pin,HIGH);              //Step the voltage high on conductor 1.
   read_high = analogRead(analog_pin);        //Measure response of conductor 2.
   delayMicroseconds(100);            //Delay to reach steady state.
   digitalWrite(tx_pin,LOW);               //Step the voltage to zero on conductor 1.
   read_low = analogRead(analog_pin);         //Measure response of conductor 2.
   diff = read_high - read_low;       //desired answer is the difference between high and low.
 sum += diff;                       //Sums up N_samples of these measurements.
 }
  return sum;
}                         //End of tx_rx function.


void loop() {

result = tx_rx();
result = map(result, 6000, 11000, 0, 1024);  //I recommend mapping the values of the two copper plates, it will depend on their size
Serial.println(result);
delay(100);
}
```

### Serial Communication between Arduino and Processing (From Adrián)

To open the communication between Arduino and Processing in the same computer, we could use **serial number** to transfer the Serial printing data. Same **baud rate** are also essential to make sure they are at the same frequency.

<img class="img-fluid pb-3" style="width: 40rem;" src="../serial1.png" alt=""> 
<img class="img-fluid pb-3" style="width: 40rem;" src="../serial2.png" alt=""> 


## Step Response Performance

#### Transmit-Receive
In this video, you can see the transmit-receive effect between a sponge.

<video class="img-fluid" controls width="800">
    <source src="../press1.mp4" type="video/mp4">
</video>

#### Loading

In this video, you can see the distance affect the number we got. The weight of the stuffs we put on the copper sheets could also be measured.

<video class="img-fluid pb-3" controls width="800">
    <source src="../loading1.mp4" type="video/mp4">
</video>

## Challenges


1. No **symble and footprint** of 02x02 Connector

The lastest version KiCAD for mac excluded default library, thus I couldn't find the symble of 02x02 Connector.


**!! Sollution:**
Download the KiCAD library from [official github](https://github.com/KiCad). And add the componnect in the symble and footprint library.
<img class="img-fluid pb-3" style="width: 50rem;" src="../challenge4.png" alt=""> 
<img class="img-fluid pb-3" style="width: 50rem;" src="../challenge5.png" alt=""> 

Assign the correct footprint to the connector.

<img class="img-fluid pb-3" style="width: 50rem;" src="../challenge6.png" alt=""> 

2. The **Clearance and Track Width** is too small to mill clearly.

<img class="img-fluid pb-3" style="width: 40rem;" src="../challenge2.JPG" alt="">  

**!! Sollution:**

<img class="img-fluid pt-3" style="width: 40rem;" src="../challenge1.png" alt=""> 

In PCB Layout editor increase the clearance and track width to 0.4mm. And press, the following shortcut to edit the previous layout.

- E for edit
- I for select all

3. The trace on PCB was too thin to taken off.


<img class="img-fluid pb-3" style="width: 20rem;" src="../challenge3.JPG" alt=""> 

**!! Sollution:**
Decrease the **offset** setting in Mods, to increase the thickness of connection path on PCB.
<img class="img-fluid pt-3" style="width: 50rem;" src="../challenge7.png" alt=""> 


## Download

<img class="img-fluid pb-3" style="width: 40rem;" src="../my10.png" alt="">

- [Step Response Schematic](../StepResponse.zip)

- [Step Response traces](../StepResponse_traces.svg)

- [Step Response Interior](../StepResponse_interior.svg)

<img class="img-fluid pb-3" style="width: 40rem;" src="../download1.png" alt="">

- [Arduino Step Response TX/RX (From Adrián)](../Arduino_StepResponse.zip)

- [Processing Step Response TX/RX (From Adrián)](../Processing_StepResponse.zip)



