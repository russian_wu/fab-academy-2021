+++
title ="Week 10/11: Mechanical and Machine Design"
+++

<img class="img-fluid pb-3" src="../present.JPG" alt="present">


## Assignment

- Group assignment:
  - Design a machine that includes **mechanism + actuation + automation**
  - Build the mechanical parts and operate it manually
  - Actuate and automate your machine
  - Document the group project and your individual contribution

- [Link to Group Repository](https://gitlab.com/aaltofablab/machine-building-2021-group-a)

## Inspiration

PLA filaments are the main materials we used to 3D printing, but whenever we change the colors or print out something wrong the leftover couldn't be used anymore. Base on this, we found a [Filabot](https://www.filabot.com/collections/filabot-core/products/filabot-original-ex2) in our storage that ables to recyle the leftover of 3D print. But we need to roll it up to spool **manually**, which might lead to the **uneven thickness**. It might be a good idea if we have a machine to wind the filaments **automatically** and also the **tighten** it.

<img class="img-fluid pb-3" style="width: 60rem;" src="../inspiration1.JPG" alt="">

We found two references on the Thingiverse: [LYMAN FILAMENT SPOOL WINDER](https://www.thingiverse.com/thing:30705) and [Lyman's Spool Winder Tweaks](https://www.thingiverse.com/thing:44262).

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../inspiration5.png" alt="">
      LYMAN FILAMENT SPOOL WINDER
    </div>
    <div class="col">
      <img class="img-fluid" src="../inspiration6.png" alt="">
      Lyman's Spool Winder Tweaks
    </div>
</div>

So a holder for the spool is necessary our **filament winder station**, and I refered to Ultimaker 3.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../inspiration3.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../inspiration4.JPG" alt="">
    </div>
</div>

It would be good if the holder can support the spool and be rotated by a screw. I refered to a snap joint that we learned from **Week04 Computer-Controlled Cutting**, and also found a reference from **[Snap-Fit Joints for Plastics](http://fab.cba.mit.edu/classes/S62.12/people/vernelle.noel/Plastic_Snap_fit_design.pdf)**. That I knew the snap joint should include a hook and leaning to the to outside which helps to increase the tighteness.

<img class="img-fluid pb-3" style="width: 60rem;" src="../inspiration8.png" alt="">

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../inspiration2.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../inspiration7.png" alt="">
    </div>
</div>

## Modeling

#### Holder

Base on the measurements, I designed the holder in Fusion 360, and printed it with **Ultimaker 3**.

<img class="img-fluid" src="../model1.jpg" alt="">
<img class="img-fluid" src="../model4.png" alt="">
<img class="img-fluid" src="../model11.png" alt="">

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" style="width: 30rem;" src="../model2.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" style="width: 30rem;" src="../model3.JPG" alt="">
    </div>
</div>

After taking the support off, besides some material shrunk problems which I will explian how I fixed in my **Challenges chapter** the holder fitted the spool so well and works as it should be.

<div class="row">
    <div class="col">
    <img class="img-fluid pb-3" style="width: 30rem;" src="../model5.JPG" alt="">
    </div>
    <div class="col">
      <video class="img-fluid pb-2" controls width="800">
        <source src="../model6.mp4" type="video/mp4">
      </video>
    </div>
</div>

#### Roller

<img class="img-fluid" src="../model7.jpg" alt="">
<img class="img-fluid" src="../model12.png" alt="">

<div class="row py-3">
    <div class="col">
    <img class="img-fluid" src="../model8.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../model9.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../model10.JPG" alt="">
    </div>
</div>

#### Joint with station

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../joint1.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../joint2.JPG" alt="">
    </div>
</div>

#### Filament Winder Station

For the machine, we need a staion as the body to install motors, a roller and a spool. In order to design the staion precise and properlly, the measurements for all the elements are critical here. I found a datasheet for the **step motors** from [AliExpress](https://www.aliexpress.com/item/32878514187.html), at the meanwhile I measure the motor by myself in case that has some inaccuracies to the productions.

<img class="img-fluid" src="../fila18.JPG" alt="">
<img class="img-fluid" src="../fila1.png" alt="">
<img class="img-fluid" src="../fila2.png" alt="">
Add up some simulation components to visualize the machine in Fusion 360.
<img class="img-fluid" src="../fila3.png" alt="">

Also don't forget to assign **dogbone** to assemble the all the components successfully.

<img class="img-fluid" src="../fila4.png" alt="">

From the **week 08: Computer-Controlled Machining**, I learned how to cut the wood with CNC machine. To cut it I need to create **toolpath** in Fusion.

<img class="img-fluid" src="../fila5.png" alt="">

#### Filament Winder Station *Stand Part*

<img class="img-fluid py-3" style="width: 40rem;" src="../fila6.png" alt="">

**This part is critical!!** For engraving the pockets, cutting outlines and drilling correctly, I used 3 different bits and 5 toolpaths to realize it.

<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col">Bit Size</th>
      <th scope="col">Part</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">3mm drilling bit</th>
      <td>Screw Holes</td>
    </tr>
    <tr>
      <th scope="row">3mm milling bit</th>
      <td>Pockets</td>
    </tr>
    <tr>
      <th scope="row">6mm milling bit</th>
      <td>Outlines</td>
    </tr>
  </tbody>
</table>

<img class="img-fluid pb-3" style="width: 50rem;" src="../fila7.JPG" alt="">
<img class="img-fluid pb-3" style="width: 50rem;" src="../fila8.png" alt="">

<div class="row pb-3">
    <div class="col">
     <video class="img-fluid" controls width="800">
        <source src="../fila9.mp4" type="video/mp4">
        </video>
      Drilling Screw Holes
    </div>
    <div class="col">
    <video class="img-fluid" controls width="800">
        <source src="../fila10.mp4" type="video/mp4">
    </video>
    Milling outlines
    </div>
</div>

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../fila11.JPG" alt="">
    Engrave Pockets
    </div>
    <div class="col">
    <img class="img-fluid" src="../fila12.JPG" alt="">
    Drill Screw Holes
    </div>
    <div class="col">
    <img class="img-fluid" src="../fila13.JPG" alt="">
    Cut Outlines
    </div>
</div>

#### Filament Winder Station *Bottom Part*

The bottom part is easier, because I only need three cutted-through pockets and outline. One toolpath and one 6mm milling bit can realize everything at once. The difference is my material, I selected a MDF wood from leftover that needs different **Chip Load Values(Feed per Tooth) and Cutting feedrate** because it's softer.

<img class="img-fluid pb-3" style="width: 40rem;" src="../../week08-computer-controller-machining/tool6.JPG" alt="">

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../fila16.png" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../fila17.png" alt="">
    </div>
</div>

<div class="row pb-3">
    <div class="col">
    <video class="img-fluid" controls width="800">
        <source src="../fila14.mp4" type="video/mp4">
    </video>
    </div>
    <div class="col">
    <img class="img-fluid" src="../fila15.JPG" alt="">
    </div>
</div>

And we assembled all the components successfully!!
<img class="img-fluid py-3" src="../present.JPG" alt="">


## Challenges

1. This was my first time to use **Luzbot mini**, however the performance wasn't really good. The support was printed failed, which led to the some **falls** on the hanging part. The size wasn't measured correctly, thus it didn't fit our spool. 

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid pb-2" style="width: 50rem;" src="../challenge11.png" alt="">
    </div>
    <div class="col">
    <img class="img-fluid pb-2" style="width: 30rem;" src="../challenge1.JPG" alt="">
    </div>
</div>

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../challenge2.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge3.JPG" alt="">
    </div>
</div>

The worst was the printed direction couldn't afford the **horizontal force**.

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../challenge4.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge5.JPG" alt="">
    </div>
</div>

**!! Solution:**
Changed the **printing direction** and increased the **infill percentage to 80%**.

2. The 3D printer couldn't stick on the heating platform, which led the prints detached and failed printing.

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../challenge6.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge7.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge8.JPG" alt="">
    </div>
</div>

**!! Solution:**
It might because of wrong tools to took off the prints previously which led to the tessalon platform was detroied.
Cleaning the platform gently and use some glue before printing would help the prints to attach on the stage.  

<img class="img-fluid pb-3" style="width: 30rem" src="../challenge9.JPG" alt="">

3. The 3D printed result was **shrunk** and the support in screw hole was hard to clean so that the screw couldn't fit in.

**!! Sollution:**
I created a **cube test** model, in order to know how much it shrunk to add a shrink tolerance for our model.  
<img class="img-fluid" style="width: 30rem" src="../challenge10.JPG" alt="">
<img class="img-fluid" style="width: 60rem" src="../challenge17.png" alt="">

**Rubbing knife** helped me to clean the **screw hole** really well.  
<img class="img-fluid py-3" style="width: 30rem" src="../challenge15.JPG" alt="">

**A hot air gun, hammer and a piece of cylinder wood** helped me to hammer the screw into our holde properly without any unneccessary hurt.

<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../challenge14.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge16.JPG" alt="">
    </div>
</div>

4. The support of our holder is too complicated to take off after 3D printing.

**!! Sollution:**
The stratagy is that separating a model into two parts and connect it later.
<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../challenge22.png" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge12.JPG" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge13.JPG" alt="">
    </div>
</div>


5. The pocket engraved by CNC machine was too small to fit the bearing. Drilling bit wasn't really went through the materials.

**?? Reasons and !! Sollutions:**
The **radius stock to leave** was selected. The drilling function would left less than 1mm on the bottom of stock. To drill through the whole material **Bottom Height offset** is neccessary.
<div class="row pb-3">
    <div class="col">
    <img class="img-fluid" src="../challenge18.png" alt="">
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge19.png" alt="">
    </div>
</div>

<span style="color: red"><b>!! The little tip that saves my time!!:</b></span>

If the material allows always locate it at the **END of CNC machine**, which help me to relocate my materials although I already took my material away from it.

<div class="row pb-3">
    <div class="col">
      <video class="img-fluid" controls width="800">
        <source src="../challenge20.mp4" type="video/mp4">
    </video>
    </div>
    <div class="col">
    <img class="img-fluid" src="../challenge21.JPG" alt="">
    </div>
</div>

6. The bearing and screw wasn't tight so that the station was wobble.

**!! Sollutions:**
**Electric retardant tapes** helped me to fill up the gap between screws bearings.  
<img class="img-fluid py-3" style="width: 30rem;" src="../challenge23.JPG" alt="">


## Download

<img class="img-fluid" style="width: 40rem;" src="../download1.jpg" alt="">

- [Holder for spool.stl](../SpoolHolder.stl)
- [gcode for Holder](../UMS3_SpoolerHolder.ufp)
- [Roller.stl](../Roller.stl)
- [gcoed for Roller](../LM04_Roller.gcode)
- [Spool Holder.f3d](../SpoolerHolder.f3d)

#### Filament Winder Station *Stand Part*

<img class="img-fluid pb-3" style="width: 40rem;" src="../fila6.png" alt="">

- [Filament Winder Station.f3d](../FilamentWinderStation.f3d)
1. [Toolpath 3mm Flat End](../1-StationStand_3mm-flat-end.tap)
2. [Toolpath 3mm Drilling](../2-StationStand_3mm-Drilling.tap)
3. [Toolpath 6mm Flat End](../3-StationStand_6mm-Flat-End.tap)

#### Filament Winder Station *Bottom Part*

<img class="img-fluid pb-3" style="width: 40rem;" src="../fila17.png" alt="">

- [Toolpath 6mm Flat End](../StationBottom_6mm-Flat-End.tap)

#### Cube Test

<img class="img-fluid" style="width: 40rem;" src="../download2.jpg" alt="">

- [Cube Test.stl](../cube-test.stl)

