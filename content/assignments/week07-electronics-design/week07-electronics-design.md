+++
title ="Week 07: Electronics Design"
+++
<img class="img-fluid pb-3" src="../present.jpg" alt="present">


## Assignment
- Group assignment:
  - Use the test equipment in your lab to observe the operation of a microcontroller circuit board (**Continuity Test**)
- Individual assignment:
  - Redraw an echo **hello-world board**, add (at least) a button and LED (with current-limiting resistor)
  - Check the design rules, make it, and test it
  - Extra credit: simulate its operation

## Group Assignment

- Group Members: Dinan, Eero, Onni, 吳若玄 (Russian Wu)

#### Multimeter

<video class="img-fluid pt-2" controls width="800">
  <source src="../multimeter.mp4" type="video/mp4">
</video>

We used a multimeter to do the **continuity test** of the hello-board as shown in the video. We turned the knob to select **ohm** and pressed the orange setup button to change the mode to make beeping sound when two ends connect. 

#### Power Supply 

<video class="img-fluid pt-2" controls width="800">
  <source src="../power.mp4" type="video/mp4">
</video>

We used two jumper wires to connect the power supply machine to FTDI on the board. One wire connected to the ground pin and the other connected to VCC. In this case, the power supply replaces the FTDI connector.

#### Oscilloscope 

<img class="img-fluid pb-3" style="width: 30rem;" src="../osc3.JPG" alt="">
<img class="img-fluid" style="width: 30rem;" src="../osc4.JPG" alt="">  

The introduction of the oscilloscope could be found from [here](https://www.youtube.com/watch?v=CzY2abWCVTY).
An oscilloscope is an electronic test instrument that graphically displays varying signal voltages. By using it, we could know how electricity works in reality.

<video class="img-fluid pt-2" controls width="600">
  <source src="../osc1.mp4" type="video/mp4">
</video>

This video shows the voltage wave from the TX pin of an PCB which was programmed with the [code](https://gitlab.com/kriwkrow/hello-t412) by Kirs. It is sending 1 (pressed button) and 0 (released button) to the serial monitor. So what we see in  the oscilloscope is the visualization of the 1 and 0 bytes.

<img class="img-fluid" style="width: 50rem;" src="../osc5.png" alt="">

<video class="img-fluid pt-2" controls width="600">
  <source src="../osc2.mp4" type="video/mp4">
</video>

With the same program, we attached the oscilloscope pins on **ground and LED**. The different frequencies between on and off of the button are obvious. While we pressed the button, the frequency of voltage went to 2 periods / 500ms, so that the LED blinked slowly. While the button was released, it boomed up to around 22 periods/ 500ms, which led to LED blinks really quick.

***

## Individual Assignment

#### Demonstration

<video class="img-fluid pb-3" controls width="800">
  <source src="../demonstration1.mp4" type="video/mp4">
</video>


<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../demonstration3.png" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../demonstration2.png" alt="">
      3D view
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../demonstration4.JPG" alt="">
    </div>
</div>

## KiCAD

#### Preparation

This week we designed a PCB hello-world board by ourselves. [KiCAD](https://kicad.org) is a free software that allows us to do it. For start the KiCAD smoothly, we need a [library](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) that created by Krisjanis Rijnieks. Thank you Kris :)!

<div class="row">
  <p><b>ATtiny412</b> is our starting point. For that we need the datasheet of it, we could find it <a href="https://github.com/SpenceKonde/megaTinyCore">here</a>.</p>
    <div class="col">
      <img class="img-fluid pb-3" src="../prepare1.jpg" alt="">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../prepare2.png" alt="">
    </div>
</div>

The most important pitcure we need from the datasheet is this.
<img class="img-fluid pt-1" src="../prepare3.gif" alt="">
*Picture from [SpenceKonde](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md).*

All the <span style="color:green;">green pins</span> are programalbe. <span style="color:red;">UPDI</span> is programing pin that we use for programing. <span style="color:green;">PA6 and PA7</span> are also important because **TX and RX** stands for communitcation.

#### Setting for Library
1. Preferences → Manage Symbol Library
<img class="img-fluid pt-1" src="../set1.png" alt="">

2. Preferences → Manage Footprint Library
<img class="img-fluid pt-1" src="../set2.png" alt="">

3. Preferences → Configure Paths
<img class="img-fluid pt-1" src="../set3.png" alt="">

#### !Start

- **Keyboard Shortcuts:** 
  - **M** move
  - **A** add
  - **W** wires
  - **L** label
  - **R** rotate
  - **C** copy

1. Collect all the elements we need by **Add** and search by **Fab+name**. By right click on the symbol, we can see **Symbol Properties** which includes **Datasheet, Footprint... an so on.**
<img class="img-fluid pt-1" src="../kicad1.png" alt="">

2. Then start to specify all the connection by **<span style="color: green;">Wires</span>** and **Labels**. It's an ellgant way to connect the wires. During this procedure, we can check the picture from [ SpenceKonde](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md) that we showed before to understand the connection.
<img class="img-fluid pt-1 pb-3" src="../kicad2.png" alt="">
**Hints: Labels** need to connected on the end of **<span style="color: green;">Wires</span>**.  
<img class="img-fluid" style="width: 30rem;" src="../kicad3.png" alt="">

3. Once we have done, we can see this. <span style="color: red;">Red squares</span> are the addtional parts that I did for the assignment.
<img class="img-fluid pt-1" src="../kicad4.png" alt="">
**Hints:** <span style="color: red;">TX and RX</span> need to be swapped for correct communication between computer and chips.  
<img class="img-fluid" style="width: 30rem;" src="../kicad5.png" alt="">

4. To debug our board, we need to **Annotate and Check electrical rules**  
<img class="img-fluid" src="../kicad6.png" alt="">
<img class="img-fluid pt-1" src="../kicad7.png" alt="">

- **Annotate meaning:**
  - J jumper
  - U integrated circuit
  - SW switch
  - D diode
  - R Resistor

**Hints:** Add **power flags** on input and output to pass the electrical checker.  
<img class="img-fluid" style="width: 30rem;" src="../kicad8.png" alt="">

5. Assign PCB footprints  
<img class="img-fluid py-1" style="width: 30rem;" src="../kicad9.png" alt="">
<img class="img-fluid" src="../kicad10.png" alt="">

6. Generate Netlist and Open PCB editor  
<img class="img-fluid py-1" style="width: 30rem;" src="../kicad11.png" alt="">  
<img class="img-fluid py-1" style="width: 30rem;" src="../kicad12.png" alt="">

7. **Puzzle time!** We need to find out the best layout for our PCB by practice and experience.
<img class="img-fluid pb-2" src="../kicad13.png" alt="">
**Hints:** Edit Net Classes into our bits thickness 
<img class="img-fluid " src="../kicad14.png" alt="">

8. After continously trying we got this!  
<img class="img-fluid py-3" style="width: 40rem;" src="../kicad15.png" alt="">

9. Manage ground (**GND**) in the last step   
<img class="img-fluid py-3" style="width: 5rem;" src="../kicad16.png" alt="">  
<img class="img-fluid py-3" style="width: 50rem;" src="../kicad17.png" alt="">

10. Check the design rule and Ready to export    
<img class="img-fluid py-3" style="width: 40rem;" src="../demonstration3.png" alt="">
<img class="img-fluid py-3" src="../kicad18.png" alt=""> 

11. Export into the SVG file with **margin**  
<img class="img-fluid py-3" src="../kicad19.png" alt="">
<img class="img-fluid py-3" style="width: 30rem;" src="../kicad20.png" alt="">

12. Follow same procedure we did in week05, use [mods](http://mods.cba.mit.edu/) to create the rml files.
<img class="img-fluid py-3" src="../kicad21.png" alt="">
<img class="img-fluid py-3" src="../kicad22.png" alt="">
<img class="img-fluid py-3" src="../kicad23.png" alt="">
<img class="img-fluid py-3" src="../kicad24.png" alt="">

13. Milling with stronger bits, it is more efficient.
<div class="row">
    <div class="col">
      <video class="img-fluid pb-2" controls width="800">
        <source src="../kicad25.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col">
      <video class="img-fluid pb-2" controls width="800">
        <source src="../kicad26.mp4" type="video/mp4">
      </video>
    </div>
</div>
<img class="img-fluid py-3" style="width: 30rem;" src="../kicad27.JPG" alt="">

14. Soldering patiently and gently.  
<img class="img-fluid py-3" style="width: 30rem;" src="../kicad28.JPG" alt="">

## Load a program and test my board

The setting up UPDI Programming Toolkit tutorial could be found from [here](https://www.youtube.com/watch?v=grrrgrOtLfU).  
Test file could be found from [here](https://gitlab.com/kriwkrow/hello-t412).  
<img class="img-fluid pt-2" style="width: 30rem;" src="../load1.png" alt="">

<video class="img-fluid pb-3" controls width="800">
  <source src="../demonstration1.mp4" type="video/mp4">
</video>

Cofirm of the connection of both PCBs.  
<img class="img-fluid pb-3" style="width: 40rem;" src="../load2.png" alt="">

Download the megaTinyCore from the library.  
<img class="img-fluid pb-3" style="width: 40rem;" src="../load3.png" alt="">

Select the correct board and programmer.  
<img class="img-fluid pb-3" style="width: 30rem;" src="../load4.png" alt="">
<img class="img-fluid pb-3" style="width: 40rem;" src="../load5.png" alt="">

## Challenges
1. Couldn't find **power flag** in library.<br>
**Solution:** The power flag library could be found [here](https://kicad.github.io/symbols/power).
2. Wrong switch symbol.  
<img class="img-fluid py-2" style="width: 40rem;" src="../challenge4.png" alt="">  
**Solution:**  
<img class="img-fluid py-2" style="width: 40rem;" src="../challenge1.png" alt="">
3. The **mounting holes** of the switch needs to be drilled additionally.  
<img class="img-fluid py-2" style="width: 20rem;" src="../challenge2.png" alt=""><br>
**Ellegant Solution:** pick out the yellow circles from SVG file with **Inkscape** and create rml file with mods to drill it with CNC machine.  
**Tips:** Illustrator doesn't work really well with SVG file.  
<img class="img-fluid py-2" style="width: 40rem;" src="../challenge3.png" alt="">

## Design File
- SVG files [trace](../hello-t412-F_trace.svg) and [interior](.hello-t412-Dwgs_interior.svg)
- rml files [trace](../cutting-0mm4-20mmx.rml) and [interior](../cutting-1mm5-4mmx.rml)
- [KiCAD file](../hello-t412.zip)

