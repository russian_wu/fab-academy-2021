+++
title ="Week 14: Output Devices"
+++

<div class="row pb-3">
    <div class="col-8">
      <img class="img-fluid pb-3" src="../revit2.jpg" alt="">
    </div>
    <div class="col-4">
      <video class="img-fluid" controls width="">
        <source src="../demo2.mp4" type="video/mp4">
      </video>
    </div>
</div>



## Assignment

- Individual assignment:
  - Add an output device to a microcontroller board you've designed,
  - Program it to do something

- Group assignment:
  - Measure the power consumption of an output device

## Group Assignment


***

## Individual assignment

This week with Kris's suggestion I chose **DC motor driver** and **Full-Bridge** to control the motor. In my final project I need the pump to provide air into the model, it's a good point to start it.

<img class="img-fluid pb-3" style="width: 40rem;" src="../pump1.JPG" alt="">

### Schematic

<img class="img-fluid pb-3" src="../schematic1.png" alt="">

In this week, the PCB included some new componnents: **Full-Bridge PWM Motor Drivers, Voltage Regulator and DC Power Jack**.

1. A4952 Full-Bridge DMOS PWM Motor Drivers ([Datasheet](https://www.allegromicro.com/~/media/Files/Datasheets/A4952-3-Datasheet.ashx))

<img class="img-fluid pb-3" src="../schematic2.png" alt="">

A4952 are designed to operate DC motors. The current from the output full bridge is regulated with pulse width modulated(PWM). Through the function in Full-Bridge the logic current could transform into PWM which are able to simulate the anologe signal by switching digital signal on and off at a fast rate.


<img class="img-fluid pb-3" style="width: 50rem;" src="../schematic3.png" alt="">

↑The <span style="color: red">red square</span> in the picture are mainly used in my later code. 

↓The Decoupling Capacitors I have explained in [Week 12: Input Devices](../../week12-input-devices/week12-input-devices). To stablize the voltage, we need the decoupling capacitors, but I used **0.1uF and 10uF** instead to minize the space 100uF capacitor occupies on PCB.

<img class="img-fluid pb-3" style="width: 50rem;" src="../schematic4.png" alt="">

- **Tips for soldering Full Bridge**

First melt thin layers of soldering iron both on the pad of A4952 and the PCB. Second, stablizing it with a clipper on the board. Third, heating the position of A4952 up by hot air gun.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../schematic6.JPG" alt="">
      <img class="img-fluid" src="../schematic7.jpg" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../schematic13.JPG" alt="">
    </div>
</div>


2. Voltage Regulator ([Datasheet](https://www.onsemi.com/pdf/datasheet/ncp1117-d.pdf))

<img class="img-fluid pb-3" style="width: 20rem;" src="../schematic5.png" alt="">

The explaination for **Voltage Regulator** from Week 12: Input Devices by Kris could be found on [13:56](https://youtu.be/cq9s-X3al94?t=836). As I need 5V for operating my PCB and 12V for driving the motor, regulator can help me to achieve that. In my case, I skiped the capacitors on the both sides to simplify the schematic design.

3. DC Power Jack ([Datasheet](https://www.cuidevices.com/product/resource/pj-002ah-smt-tr.pdf))

### PCB Layout
<img class="img-fluid pb-3" style="width: 60rem;" src="../schematic8.png" alt="">

#### Additional holes for Power Jack and Conntector.

<img class="img-fluid pb-3" style="width: 30rem;" src="../schematic9.png" alt="">

The **Wire to Board Terminal** needs **Rivets** to connect and the power jack need mounting holes for steady. I drew them addtionally with the help of Inkscape. **Not by margin layer, because the position would change in different import and export on Inkscape.**

<img class="img-fluid pb-3" src="../schematic14.JPG" alt="">


### Create toolpath and mill

<img class="img-fluid" src="../schematic10.png" alt="">
<img class="img-fluid" src="../schematic11.png" alt="">

<img class="img-fluid pb-3" style="width: 30rem;" src="../schematic12.jpg" alt="">

### Revits and Soldering

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid"  src="../revit1.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid"  src="../revit3.JPG" alt="">
    </div>
</div>

<img class="img-fluid pb-3" style="width: 50rem;" src="../revit2.jpg" alt="">

### Cables for the Motor

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid"  src="../cable1.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid"  src="../cable2.JPG" alt="">
    </div>
</div>

The **soldering iron-coated** cables enable them connect to the **wire to board terminal** easier. 

### Coding

What I am doing in the code is created two funcions **Forward and Reverse** to rotate the motor gradually up to full speed and decrease to 0 speed by changing the **analogWrite** of IN1 and IN2 pins. The Forward and Reverse could refer to **PWM Control Truth Table** from [Full-Bridge datasheet]((https://www.allegromicro.com/~/media/Files/Datasheets/A4952-3-Datasheet.ashx)).

<img class="img-fluid pb-3" style="width: 50rem;" src="../coding1.png" alt="">

```
#define motorIN1 0 // IN1
#define motorIN2 1 // IN2
    
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(motorIN1, OUTPUT); //In hole
  pinMode(motorIN2, OUTPUT); //Out hole
}

// the loop function runs over and over again forever
void loop() {
  Forward();
  Reverse();
}

void Forward(){
  
    digitalWrite(motorIN2, 0);
//  digitalWrite(motorIN1, 1); testing
//  delay(5000);

  for (int i=0; i<256 ; i++)
  {
    analogWrite(motorIN1,i);
    delay(30); 
  }
  
  for (int i=255; i>=0 ; i--)
  {
    analogWrite(motorIN1,i);
    delay(30); 
  }
  
  digitalWrite(motorIN1, 1); //break
  digitalWrite(motorIN2, 1); //break
  delay(100);
  
  digitalWrite(motorIN1, 0); //standby
  digitalWrite(motorIN2, 0); //standby
  delay(100);
}

void Reverse(){
  digitalWrite(motorIN1, 0);
  
  for (int i=0; i<256 ; i++)
  {
    analogWrite(motorIN2,i);
    delay(30); 
  }
  
  for (int i=255; i>=0 ; i--)
  {
    analogWrite(motorIN2,i);
    delay(30); 
  }

  digitalWrite(motorIN1, 1); //break
  digitalWrite(motorIN2, 1); //break
  delay(100);

  digitalWrite(motorIN1, 0); //standby
  digitalWrite(motorIN2, 0); //standby
  delay(100);
}
```

### The functioning motors demonstration

<div class="row pb-3">
    <div class="col-9">
      <video class="img-fluid" controls width="">
        <source src="../demo1.mp4" type="video/mp4">
      </video>
    </div>
    <div class="col-3">
      <video class="img-fluid" controls width="">
        <source src="../demo2.mp4" type="video/mp4">
      </video>
    </div>
</div>


## Challenges


1. The holes were too hugh for the vias to hold.

<img class="img-fluid pb-3" style="width: 40rem;" src="../challenge1.JPG" alt="">

**!! Sollution:** Draw the holes on Inkscape from **F.Cu** layer. It could not only fix the holes position but also faster to modify.

<img class="img-fluid pb-3" src="../schematic14.JPG" alt="">
   
2. The first USB UPDI was broken; therefore, I couldn't program my DC Motor Driver.

<div class="row pb-3">
    <div class="col-7">
      <img class="img-fluid" src="../challenge6.png" alt="">
    </div>
    <div class="col-5">
      <img class="img-fluid" src="../challenge2.JPG" alt="">
    </div>
</div> 

**!! Sollution:** Build a new one, it might need to be casted with a layer of expoxy.

<img class="img-fluid pb-3" style="width: 30rem;" src="../challenge3.jpg" alt="">

3. The Forward function couldn't speed up and down gradually.


**?? Reason:**
The IN1 was connected to Pin2 (<span style="color: green">PA6</span> / <span style="color: brown"> Arduino 0</span>), however Pin2 couldn't receive **~PWM** signal. Thus the forward function could only perform <span style="color: red">turn on/off</span> in full speed.

<img class="img-fluid pb-3" style="width: 30rem;" src="../challenge4.png" alt="">
<img class="img-fluid pb-3" src="../challenge5.gif" alt="">



## Download

<img class="img-fluid pb-3" style="width: 30rem;" src="../download1.png" alt="">

- [DCMotor-Driver](../DCMotor-Driver.zip)

<img class="img-fluid pb-3" style="width: 30rem;" src="../schematic8.png" alt="">

- [Schematic](../H-bridge.zip)


