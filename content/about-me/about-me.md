+++
title = "About Me"
lead = "This is my Fab Academy documentations!"
+++

### Russian Wu

<img class="img-fluid py-2" src="../profile.jpg" alt="Profile" >

Moi! My name is Russian, but unfortunately I am not a Russian. :D I come from Taiwan, now I am an exchange student in the Aalto University.



### Motivation

In the few semesters ago, I built a project called **[Sound Sculpture](https://youtu.be/R5w4qhq77rw)**. It reacts to the environment sound with different level of LED. The idea is to visuallize the sound pollution that hides in the metropolis, which erodes our mental health "in slience." This was my first date with Arduino and soldering stuffs. Besides, I always see the 3D printers sit in the laboratory, but I never know how to start it. I think Digital Fabrication would be a nice start point.  

What I have to do now is finding some interesting ideas to build up!

### Contact me:
Email: ruo-xuan.wu@aalto.fi