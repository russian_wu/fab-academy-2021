+++
title ="Final Project"
+++

## Virtual Hug / Inflatable Suit

Inspiration from the corona situation and TV series, [Upload](https://www.imdb.com/title/tt7826376/), I want to design a inflatable suit which allows people to hug remotely.  

<img class="img-fluid py-3" src="../sketch.jpg" alt="sketch">


## Social Contact

After the outbreak of Corona pandemic, quarantines and staying at home become a necessary measure to prevent the spreading of the virus. To fulfill the need of **social contact**, we login social meida more often than before subconsciously. We want to connect with others. However, many researchs indicate that ["Social media may promote negative experiences"](https://www.helpguide.org/articles/mental-health/social-media-and-mental-health.htm). More time we spend on the social media, higher risk to ddeveloping **anxiety and depression**.

## We don't hug anymore

To reduce the possibilty of infection, rather than greeting with hug, people greet with elbows. But comparing with hug, it stil lacks some of warm.

In the last Chrismas, some families couldn't visit their grandparents for not giving the virus any chance to hurt who they love. 

For long-distance relationships and students who study abroad, meeting with their family and partner online isn't a challenge anymore. But we still couldn't touch each others.

## Inspiration

In the TV series [Upload](https://www.imdb.com/title/tt7826376/), **Sex Suit** allows the people to contact with people who live in the virtual world physically. Here is the short cut of it:

<video class="img-fluid pb-37ilrgwsㄍㄧㄤ" controls width="720">
	<source src="../sex-suit.mp4" type="video/mp4">
</video>

The inside of Sex Suit look like this:
<iframe class="mg-fluid pb-3" width="720" height="480" src="https://www.youtube.com/embed/OyqyQHx2N_c?start=7" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Possibilty

In the Project, [Pneumatic Surface](http://fab.cba.mit.edu/classes/863.14/people/merav_gazit/project-16.html) inflates the surface which was made with sillicone. I would intergrate the surface under the inflatable suit to simulate the huging pressure from another model.
[Joined seat](http://fab.cba.mit.edu/classes/863.17/Architecture/people/darle/week-final.html).

## Related Product
[bHaptic](https://www.bhaptics.com) is a simlar product that I can learn from, it has already applied into VR games.

https://mab14.mediaarchitecture.org/news/natural-affect-new-type-public-infrastructure/

***

## Week 06 - 3D Scanning and Printing

<img class="img-fluid pt-3" src="../../assignments/week06-3d-scanning-and-printing/present.jpg" alt="sketch"> 

I tried to create an experimental form with grasshopper in the [Week 06](../../assignments/week06-3d-scanning-and-printing/week06-3d-scanning-and-printing).

***

## Week 07 - Structure references


<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pb-1" src="../structure3.jpg" alt="sketch">
      Orchestrating the depth of light, by Media Architecture Institute, Ludwig Maximilian Universität. 2014.
      <a href="https://www.mediaarchitecture.org/orchestrating-the-depth-of-light/">Website</a>
      <a href="https://vimeo.com/95281731">Video</a>
    </div>
    <div class="col">
      <img class="img-fluid pb-1" src="../structure4.png" alt="sketch">
      The Origin of Stripes, by MARC FORNES / THEVERYMANY. 2017.
      <a href="https://vimeo.com/201014301">Video</a>
    </div>
</div>

***

## Week 09 - Structure references

<div class="row py-3">
    <div class="col">
      <img class="img-fluid" src="../structure1.jpg" alt="sketch">
      Humanoids Family, Ernesto Neto. 2001.
    </div>
    <div class="col">
      <img class="img-fluid" src="../structure2.jpg" alt="sketch">
      "The Body That Carries Me," Ernesto Neto. February 13, 2014.
      <a href="https://elephant.art/iotd/ernesto-neto-humanoids-family-2001-21052020/">Website1</a>
      <a href="https://artdaily.cc/news/68238/-Ernesto-Neto--The-Body-that-Carries-Me--opens-at-The-Guggenheim-Museum-Bilbao#.YFiQai0Ro1J">Website2</a>
    </div>
</div>

***

## Week13 - Casting of prototype

At the [Week 13: Molding and Casting](../../assignments/week13-molding-and-casting), we learned how to build molds and cast silicone. Comparing with wax molds, I found 3D printers are the strongest molds to casting. But the size of 3D printers are usually smaller than 3D models.

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid" src="../casting1.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../casting2.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../casting3.JPG" alt="">
    </div>
</div>
<div class="row py-3">
    <div class="col">
      <img class="img-fluid" src="../casting5.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../casting4.JPG" alt="">
    </div>
    <div class="col">
      <img class="img-fluid" src="../casting6.JPG" alt="">
    </div>
</div>


***

- What does it do?
- Who's done what beforehand?
- What did you design?
- What materials and components were used?
- Where did they come from?
- How much did they cost?
- What parts and systems were made?
- What processes were used?
- What questions were answered?
- What worked? What didn't?
- How was it evaluated?
- What are the implications?






